import {Injectable} from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';
import {HTTP_INTERCEPTORS} from '@angular/common/http';

import {Observable} from 'rxjs';

import {AuthenticationService} from "./authentication.service";


@Injectable()
export class AuthInterceptor implements HttpInterceptor {


  constructor(private authenticationService: AuthenticationService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    // Get the auth token from the service.
    const authToken = this.authenticationService.getToken();

    let authReq = req.clone({
      headers: req.headers.set('callback', location.origin)
    });
    if(authToken){
      // Clone the request and replace the original headers with
      // cloned headers, updated with the authorization.
      // authToken = 'fdcd5960-6a97-4c16-b99d-af76aa2dde94';
        authReq = authReq.clone({
          headers: authReq.headers.set('Authorization', `Digest ${authToken}`)
        });
    }

    // send cloned request with header to the next handler.
    return next.handle(authReq);
  }
}

/** Http interceptor providers in outside-in order */
export const httpInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
];
