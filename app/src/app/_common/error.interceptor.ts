import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, take, retry } from 'rxjs/operators';
import 'rxjs/add/observable/throw';

import { AuthenticationService } from "./authentication.service";


@Injectable()
export class ErrorInterceptor implements HttpInterceptor {


    constructor(private authenticationService: AuthenticationService) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler) {
        // Get the auth token from the service.
        // const authToken = this.authenticationService.getToken();
        return next.handle(request).pipe(
            catchError((err, caught) => {
                console.log(caught);
                if (err.status === 401){
                    this.authenticationService.logout();
                }
                return Observable.throw(err);
        }));
    }

}

/** Http interceptor providers in outside-in order */
export const httpErrorInterceptorProvider = [
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
];
