import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Observable } from 'rxjs';

import { SuccessDialogComponent } from './success.dialog/success.dialog.component';
import { ConfirmDialogComponent } from './confirm.dialog/confirm.dialog.component';

@Injectable({
    providedIn: 'root'
})
export class ToolboxService {

    private _appUrl: string = '';

    get appUrl(): string {
        return this._appUrl;
    }

    set appUrl(url: string) {
        this._appUrl = url;
    }

    constructor(private dialog: MatDialog) { }

    showSuccess(header: string, text: string): void {
        this.dialog.open(SuccessDialogComponent, {
            width: '450px',
            data: { header: header, text: text },
            panelClass: 'dialog-success'
        });
    }

    showError(header: string, text: string): void {
        this.dialog.open(SuccessDialogComponent, {
            width: '450px',
            data: { header: header, text: text },
            panelClass: 'dialog-error'
        });
    }

    showObservableSuccess(header: string, text: string): Observable<any> {
        const dialogRef = this.dialog.open(SuccessDialogComponent, {
            width: '450px',
            data: { header: header, text: text },
            panelClass: 'dialog-success'
        });

        return dialogRef.afterClosed();
    }

    confirm(header: string, text: string, type: string = 'error'): Observable<any> {
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            width: '450px',
            data: { header: header, text: text },
            panelClass: 'dialog-' + type
        });

        return dialogRef.afterClosed();
    }

    /** Safe Base 64 Encode */
    base64Encode(str: string): string {
        return btoa(
            encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function toSolidBytes(match, p1) {
                return String.fromCharCode(parseInt('0x' + p1, null));
            })
        );
    }

    /** Safe Base 64 Decode */
    base64Decode(str): string {
        return decodeURIComponent(
            atob(str)
                .split('')
                .map(function (c) {
                    return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
                })
                .join('')
        );
    }

}
