import { TestBed, inject } from '@angular/core/testing';

import { ToolboxService } from './toolbox.service';

describe('ToolboxService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ToolboxService]
    });
  });

  it('should be created', inject([ToolboxService], (service: ToolboxService) => {
    expect(service).toBeTruthy();
  }));
});
