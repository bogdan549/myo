import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { Router } from '@angular/router';

import { environment } from './../../environments/environment';
import { Login } from './../loginComponent/classes/login.class';

const TOKEN_KEY = 'myo_access_token';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {

    constructor(private http: HttpClient,
        private router: Router) { }

    getToken(): string {
        return localStorage.getItem(TOKEN_KEY);
    }

    setToken(token: string): void {
        localStorage.setItem(TOKEN_KEY, token);
    }

    checkAccess(): boolean {
        if (!this.getToken()) {
            this.router.navigate(['/login']);
            return false;
        }
        return true;
    }

    login(loginObj: Login): Observable<any> {
        return this.http.post<any>(environment.apiUrl + 'user/authenticate', loginObj);
    }

    logout(): void {
        localStorage.removeItem(TOKEN_KEY);
        localStorage.removeItem('myo_user_role');
        this.router.navigate(['/login']);
    }

    forgotPassword(email: string): Observable<any> {
        return this.http.post<any>(environment.plainUrl + `external/forgot_password?email=${email}`,{});
    }

    restorePassword(token: string, password: string): Observable<any> {
        return this.http.post<any>(environment.plainUrl + `external/forgot_password/noforms/${token}/1`,{
            browserRequest: false,
            password: password,
            isBrowserRequest: false   // false обязательно, иначе вернет хтмл страницу
        });
    }

    register(token: string, password: string, type: string): Observable<any> {
        return this.http.post<any>(environment.apiUrl + `user/register`,{
            token: token,
            password: password,
            type: type
        });
    }

}
