import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';
import { LoginComponent } from './login.component';
import { SharedModule } from '../shared.module'; 

@NgModule({
    imports:      [ BrowserModule, FormsModule, HttpClientModule, SharedModule]
})
export class LoginModule { }
