export class Login {
    email: string;
    password: string;
    push_token: string;
    platform: Platform;

    constructor() {
        this.email = '';
        this.password = '';
        this.push_token = '';
        this.platform = Platform.android;
    }
}

export enum Platform {
    android = 'android',
    some = 'some'
}