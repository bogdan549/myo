import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { AuthenticationService } from '../_common/authentication.service';
import { Login, Platform } from './classes/login.class';
import { BreadcrumbsService } from '../breadcrumbsComponent/services/breadcrumbs.service';
import { UserService } from '../myoMainModule/_services/user/user.service';
import {CompanyService} from "../myoMainModule/_services/company/company.service";

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	private subscription: Subscription = new Subscription();
    wrongCredentials: boolean = false;
    loginForm: FormGroup = new FormGroup({
        email: new FormControl(),
        password: new FormControl()
    });

    private loginObj: Login;

    constructor(private authenticationService: AuthenticationService,
        private fb: FormBuilder,
        private router: Router,
        private breadcrumbsService: BreadcrumbsService,
        private userService: UserService,
        private companyService: CompanyService
    ) { }

    ngOnInit() {
        this.loginForm = this.fb.group({
            email: ['', Validators.required],
            password: ['', Validators.required]
        });
        this.breadcrumbsService.setStep(0, 'Login', this.router.url);

    }

    submitLoginForm(): void {
        this.wrongCredentials = false;
        if (this.loginForm.invalid) return;

        this.loginObj = new Login();
        this.loginObj.email = this.loginForm.value.email;
        this.loginObj.password = this.loginForm.value.password;
        console.log(this.loginObj);
        // localStorage.setItem('myo_access_token', '9daa6494-e781-420e-8306-64a6fb340d31');
        // this.router.navigate(['/facility']);
        this.subscription.add(
            this.authenticationService.login(this.loginObj).subscribe(response => {
                this.authenticationService.setToken(response.access_token);
                console.log(response);

              this.userService.user = response.profile;

                console.log("Checking company existance");
                this.subscription.add(
                  this.companyService.getCompanyList().subscribe(companies => {
                    if(companies.length == 0){
                      console.log("No companies found, redirecting to company creation page");
                      this.router.navigate(['/createcompany']);
                    } else {
                      //company exists
                      this.userService.user.company_id = companies[0].id;
                      this.router.navigate(['/']);
                    }
                  }, error => {
                    console.log("Can't get companies")
                  })
                );


            }, error => {
                console.log(error);
                this.wrongCredentials = true;
            })
        )

    }

    ngOnDestroy() {
		this.subscription.unsubscribe();
	}


}
