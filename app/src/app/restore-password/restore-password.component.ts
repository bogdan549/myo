import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { BreadcrumbsService } from '../breadcrumbsComponent/services/breadcrumbs.service';

@Component({
    selector: 'app-restore-password',
    templateUrl: './restore-password.component.html',
    styleUrls: ['./restore-password.component.css']
})
export class RestorePasswordComponent implements OnInit {

    // isStep1Completed: boolean = false;
    restoreToken: string;

    constructor(private router: Router,
        private breadcrumbsService: BreadcrumbsService,
        private route: ActivatedRoute) { }

    ngOnInit() {
        this.breadcrumbsService.setStep(1, 'Restore Password', this.router.url);
        this.restoreToken = this.route.snapshot.paramMap.get('token');
    }


    // restorePassword(): void {
    //     this.isStep1Completed = true;
    // }

}
