import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestoreStep2Component } from './restore-step2.component';

describe('RestorePasswordComponent', () => {
  let component: RestoreStep2Component;
  let fixture: ComponentFixture<RestoreStep2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestoreStep2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestoreStep2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
