import { Component, OnInit, Input } from '@angular/core';
import { AuthenticationService } from '../../_common/authentication.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { ToolboxService } from '../../_common/toolbox.service';

@Component({
    selector: 'app-restore-step2',
    templateUrl: './restore-step2.component.html',
    styleUrls: ['./restore-step2.component.css']
})
export class RestoreStep2Component implements OnInit {

    @Input() restoreToken: string;
    private subscription: Subscription = new Subscription();

    restoreForm: FormGroup = new FormGroup({
        password: new FormControl(),
        confirmPassword: new FormControl()
    });

    constructor(private authenticationService: AuthenticationService,
        private fb: FormBuilder,
        private toolboxService: ToolboxService,
        private router: Router) { }

    ngOnInit() {
        this.restoreForm = this.fb.group({
            password: ['', Validators.required],
            confirmPassword: ['', Validators.required]
        });

    }

    restorePassword(): void {
        if (this.restoreForm.invalid) {
            this.restoreForm.controls.password.markAsTouched();
            this.restoreForm.controls.confirmPassword.markAsTouched();
            return;
        }
        if (this.restoreForm.value.password !== this.restoreForm.value.confirmPassword) {
            this.restoreForm.controls.confirmPassword.setErrors({notEqual: true})
            return;
        }
        this.subscription.add(
            this.authenticationService.restorePassword(this.restoreToken, this.restoreForm.value.password).subscribe(() => {
                this.subscription.add(
                    this.toolboxService.showObservableSuccess('Restore poassword', 'Password successfully changed.').subscribe(() => {
                        this.router.navigate(['/login']);
                    })
                );
            })
        );
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
