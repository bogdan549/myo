import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { AuthenticationService } from '../../_common/authentication.service';
import { ToolboxService } from '../../_common/toolbox.service';

@Component({
    selector: 'app-restore-step1',
    templateUrl: './restore-step1.component.html',
    styleUrls: ['./restore-step1.component.css']
})
export class RestoreStep1Component implements OnInit {

    @Output() onCompleate: EventEmitter<boolean> = new EventEmitter();

    private subscription: Subscription = new Subscription();

    resetForm: FormGroup = new FormGroup({
        email: new FormControl()
    });

    constructor(
        private toolboxService: ToolboxService,
        private authenticationService: AuthenticationService,
        private fb: FormBuilder, ) { }

    ngOnInit() {
        this.resetForm = this.fb.group({
            email: ['', Validators.required]
        });
    }

    resetPassword(): void {
        this.resetForm.controls.email.markAsDirty();
        if (this.resetForm.invalid) return;
        this.subscription.add(
            this.authenticationService.forgotPassword(this.resetForm.value.email).subscribe(() => {
                this.onCompleate.next(true);
            }, error => {
                console.log(error);
            })
        );
        this.showInfo();
    }

    showInfo(): void {
		const header: string = 'Info';
		const text: string = 'An email has been sent to allow you to reset your password.';
		this.toolboxService.showSuccess(header, text);
	}

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
