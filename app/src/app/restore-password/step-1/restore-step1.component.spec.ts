import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestoreStep1Component } from './restore-step1.component';

describe('RestoreStep1Component', () => {
  let component: RestoreStep1Component;
  let fixture: ComponentFixture<RestoreStep1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestoreStep1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestoreStep1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
