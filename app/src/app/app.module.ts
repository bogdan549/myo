import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { MyoMainModule } from './myoMainModule/myo-main.module';
import { SharedModule } from './shared.module'; 

import { AppComponent } from './app.component';
import { SuccessDialogComponent } from './_common/success.dialog/success.dialog.component';
import { BreadcrumbsComponent } from './breadcrumbsComponent/breadcrumbs.component';
import { RestoreStep1Component } from './restore-password/step-1/restore-step1.component';
import { RestoreStep2Component } from './restore-password/step-2/restore-step2.component';
import { RestorePasswordComponent } from './restore-password/restore-password.component';
import { httpInterceptorProviders } from './_common/authentication.interceptor';
import { ConfirmDialogComponent } from './_common/confirm.dialog/confirm.dialog.component';
import { httpErrorInterceptorProvider } from './_common/error.interceptor';

@NgModule({
    imports: [
    	BrowserModule,
    	FormsModule,
    	ReactiveFormsModule,
    	HttpClientModule,
    	AppRoutingModule,
    	MyoMainModule,
    	MatDialogModule,
    	BrowserAnimationsModule,
        SharedModule
    ],
    declarations: [
        AppComponent,
        SuccessDialogComponent,
        BreadcrumbsComponent,
        RestorePasswordComponent,
        RestoreStep1Component,
        RestoreStep2Component,
        ConfirmDialogComponent
    ],
	bootstrap: [ AppComponent ],
	entryComponents: [SuccessDialogComponent, ConfirmDialogComponent],
	exports: [BreadcrumbsComponent],
  providers:[httpInterceptorProviders, httpErrorInterceptorProvider]
})
export class AppModule { }
