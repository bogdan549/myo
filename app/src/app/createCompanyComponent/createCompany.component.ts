import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { AuthenticationService } from '../_common/authentication.service';
import { BreadcrumbsService } from '../breadcrumbsComponent/services/breadcrumbs.service';
import { UserService } from '../myoMainModule/_services/user/user.service';
import {CreateCompanyRequest} from "./classes/createCompanyRequest.class";
import {CompanyService} from "../myoMainModule/_services/company/company.service";
import {EmployeeService} from "../myoMainModule/_services/employee/employee.service";
import {NewEmployee} from "../myoMainModule/employeesComponent/add-employee-dialog/_classes/new-employee.class";
import {EmployeeRole} from "../myoMainModule/_classes/employee.class";
import {ToolboxService} from "../_common/toolbox.service";

@Component({
    selector: 'createcompany',
    templateUrl: './createCompany.component.html',
    styleUrls: ['./createCompany.component.css']
})
export class CreateCompanyComponent implements OnInit {

	private subscription: Subscription = new Subscription();
    wrongCredentials: boolean = false;
    companyCreateForm: FormGroup = new FormGroup({
        name: new FormControl(),
        file: new FormControl(),
        firstName: new FormControl(),
        lastName: new FormControl(),
        email: new FormControl(),
        //password: new FormControl(),
    });

    private companyRequest: CreateCompanyRequest;

    constructor(
        private authenticationService: AuthenticationService,
        private fb: FormBuilder,
        private router: Router,
        private breadcrumbsService: BreadcrumbsService,
        private companyService: CompanyService,
        private employeeService: EmployeeService,
        private userService: UserService,
        private toolBoxService: ToolboxService
    ) { }

    ngOnInit() {
        this.companyCreateForm = this.fb.group({
            name: ['', Validators.required],
            file: [null, Validators.required],
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            email: ['', Validators.required],
            //password: ['', Validators.required]
        });

        this.breadcrumbsService.setStep(0, 'Create company', this.router.url);

    }

    resultMessage: string;

    submitForm(): void {

    }

    createAndUpload() : void {
      this.wrongCredentials = false;
      if (this.companyCreateForm.invalid) return;

      this.companyRequest = new CreateCompanyRequest();
      this.companyRequest.name = this.companyCreateForm.value.name;
      console.log(this.companyRequest);

      this.subscription.add(
        this.companyService.createCompany(this.companyRequest).subscribe(company => {
          console.log("Company created: " + company.id);
          console.log("Uploading logo...");

          let logoData = this.getLogoFormData();

          this.subscription.add(
            this.companyService.uploadLogo(company.id, logoData).subscribe(result => {
              console.log("Logo uploaded");
              console.log("Creating employee...");

              let headEmployee = this.getCompanyHeadEmployee(company.id);

              this.subscription.add(

                this.employeeService.addEmployee(headEmployee).subscribe(employee => {
                  console.log("Employee invite sent");
                  this.resultMessage = "Company creation successful, company head will get invite";
                }, error => {
                  console.log("Can't create employee");
                  this.resultMessage = "Error creating company head";
                })

              );

            }, error => {
              console.log("Can't upload logo");
              this.resultMessage = "Error uploading logo";
            })
          );

        }, error => {
          console.log("Couldn't create company");
          this.resultMessage = "Error creating company";
        })

      )
    }

    getLogoFormData(): any {
      let input = new FormData();
      input.append('file', this.companyCreateForm.get('file').value);
      return input;
    }

    onFileChange($event): void {
      let target = (<HTMLInputElement>event.target);
      if(target.files.length > 0) {
        let file = target.files[0];
        this.companyCreateForm.get('file').setValue(file);
      }
    }

    getCompanyHeadEmployee(companyId: number): NewEmployee {
      let newEmployee = new NewEmployee();
      newEmployee.firstName = this.companyCreateForm.value.firstName;
      newEmployee.lastName = this.companyCreateForm.value.lastName;
      newEmployee.email = this.companyCreateForm.value.email;
      //newEmployee.password = this.companyCreateForm.value.password;
      newEmployee.password = '';
      newEmployee.postId = companyId;
      newEmployee.dateOfBirth = "1970-01-01";
      newEmployee.hobbies = "";
      newEmployee.motivation = "";
      newEmployee.phoneNumber = "";
      newEmployee.placeOfBirth = {city: "", country: ""};
      newEmployee.previousHobbies = "";
      newEmployee.startYear = (new Date()).getFullYear();
      newEmployee.type = EmployeeRole.COMPANY_HEAD;

      return newEmployee;
    }

    logout() {
      this.authenticationService.logout();
    }

    ngOnDestroy() {
      this.subscription.unsubscribe();
    }


}
