import { Facility } from './myoMainModule/_classes/facility.class';
import { Employee, EmployeeRole } from './myoMainModule/_classes/employee.class';
import { Story } from './myoMainModule/_classes/story.class';
import { SeniorProfile } from './myoMainModule/_classes/senior-profile.class';
import { EmployeeProfile } from './myoMainModule/_classes/employee-profile.class';
import { Category } from './myoMainModule/_classes/category.class';
import { Admin } from './myoMainModule/_classes/admin.class';



// export const EMPLOYEES: Employee[] = [
// 	{
// 		id: 1,
// 		image: 'http://via.placeholder.com/100x100/ea6c6c',
// 		firstName: "Ivan",
// 		lastName: "Petro",
// 		status: "Linked",
// 		role: EmployeeRole.caregiver,
// 		sections: [
// 			{
// 				id: 0,
// 				name: "Erdgeschoss",
// 				elderlies: [
// 					{
// 						id: 0,
// 						image: 'http://via.placeholder.com/100x100/dddddd',
// 						firstName: 'GIYBBj',
// 						lastName: 'wdwfw',
// 						relatives: [
// 							'sdas',
// 						],
// 						stories: ['asdasdasasad asdas d', 'Lorem ipsum dolor sit amet, consectetur.']
// 					},
// 					{
// 						id: 1,
// 						image: 'http://via.placeholder.com/100x100/dddddd',
// 						firstName: 'Lorem ipsum.',
// 						lastName: 'fsdf',
// 						relatives: [
// 							'df adf a', 'adfasd fasd'
// 						],
// 						stories: ['Lorem ipsum dolor sit amet, consectetur.']
// 					}
// 				],
// 				relatives: 1,
// 				caretakers: ['asdas', 'asdfsdfa dsf', 'fsdfsd fasd'],
// 				posts: 3
// 			},
// 			{
// 				id: 1,
// 				name: "Ipsum sd",
// 				elderlies: [
// 					{
// 						id: 0,
// 						image: 'http://via.placeholder.com/100x100/dddddd',
// 						firstName: 'Lorem ipsum.',
// 						lastName: 'sda',
// 						relatives: [
// 							'df adf a', 'adfasd fasd', 'sdfsdf s', 'sdfsdf'
// 						],
// 						stories: ['Lorem ipsum dolor sit amet, consectetur.']
// 					}
// 				],
// 				relatives: 4,
// 				caretakers: ['asdas', 'asdfsdfa dsf', 'fsdfsd fasd', 'asdas das', 'asdas dasd a'],
// 				posts: 6
// 			}
// 		]
// 	}
// ];


export const EMPLOYEE: EmployeeProfile = {
	id: 1,
	first_name: "Lisa",
	last_name: "Ny",
	avatar: "http://via.placeholder.com/100x100/ddffff",
	email: "MyEmail@email.com",
	phone: "+498531561",
	place: "Germani",
	birthday: '07.05.1995',
	prehobby: 'Craft and gardening',
	tfacil: 'Since 2016',
	hobbi: 'Yoga, Music',
	motiv: 'Treat others how you want to be treated.',
	stories: [
		{ id: 1, name: 'Approw need' },
		{ id: 2, name: 'Approw need #2' },
		{ id: 3, name: 'Approw need #3' },
		{ id: 4, name: 'Approw need #4' }
	],
	status: [
		{ id: 1, name: 'Caregivers' },
		{ id: 2, name: 'Caretakers' },
		{ id: 3, name: 'Caresome' },
		{ id: 4, name: 'Somebody' }
	],
	facilities: [
		{ id: 1, name: 'Facility-1' },
		{ id: 2, name: 'Facility-2' }
	]
};

// export const POSTS: Story[] = [
// 	{
// 		id: 0,
// 		description: 'dfsdf sdfsd fsd sdf Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque, sapiente, deserunt. Eos.',
// 		author: 'Lorem ipsum.',
// 		category: Category.Medical,
// 		type: 'type1',
// 		isInternal: true,
// 		isUploadComplete: true,
// 		recipients: ['Frau Waltz', 'Christoph Vogel', 'Lynn Morris']
// 	},
// 	{
// 		id: 1,
// 		description: ' tdh hsit rohiojs;igjsieeofznfsdjf eifgasjnckdsfjk Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit a mollitia sunt, cumque saepe quo?',
// 		author: 'Upsum sdfsf',
// 		category: Category.Announcement,
// 		type: 'type2',
// 		isInternal: true,
// 		isUploadComplete: true,
// 		recipients: ['Lynn Morris']
// 	},
// ];

// export const ADMINS: Admin[] = [
// 	{
// 		id: 1,
// 		first_name: "Lisa",
// 		last_name: "Frangenheim",
// 		email: "lisa@wiesbaden.de",
// 		role: "Company CEO",
// 		status: "linked",
// 		facilities: "All",
// 		sections: "All",
// 	},
// 	{
// 		id: 1,
// 		first_name: "Rudolf",
// 		last_name: "Biedenkapp",
// 		email: "rudolf.biedenkapp@Seniorenwohnhaus.de",
// 		role: "Facility Head",
// 		status: "linked",
// 		facilities: "Seniorenwohnhaus",
// 		sections: "All"
// 	},
// 	{
// 		id: 1,
// 		first_name: "Oswald",
// 		last_name: "Bethke",
// 		email: "oswald.bethke@pflegernetz.de",
// 		role: "Section Head",
// 		status: "invited",
// 		facilities: "Wiesbaden Living",
// 		sections: "1. OG, 2. OG, 4. OG, Enganee, Wiliamerth"
// 	}
// ];

export const RECIPIENTS: string[] = ['Alfred Kramer', 'Frau Waltz', 'Christoph Vogel', 'Lynn Morris', 'Ulrike Fleck', 'Emil Ende'];
