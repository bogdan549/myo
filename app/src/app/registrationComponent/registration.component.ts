import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { BreadcrumbsService } from '../breadcrumbsComponent/services/breadcrumbs.service';
import { AuthenticationService } from '../_common/authentication.service';
import { ToolboxService } from '../_common/toolbox.service';
import { RegistrationType } from './_class/registration-type.enum';

@Component({
	selector: 'registration',
	templateUrl: './registration.component.html',
	styleUrls: ['./registration.component.css']
})

export class RegistrationComponent implements OnInit {

	email: string;
    private token: string;
    private type: RegistrationType;
    private subscription: Subscription = new Subscription();

	registerForm: FormGroup = new FormGroup({
        password: new FormControl(),
        confirmPassword: new FormControl()
    });

	constructor(private router: Router,
		private breadcrumbsService: BreadcrumbsService,
		private route: ActivatedRoute,
		private fb: FormBuilder,
		private authenticationService: AuthenticationService,
		private toolboxService: ToolboxService) { }

	ngOnInit() {
    let type = this.route.snapshot.paramMap.get('type');
    type = type ? type.toUpperCase() : type;
    this.type = type ? RegistrationType[type] : RegistrationType.RELATIVE;
    this.token = this.route.snapshot.paramMap.get('token');
		this.email = this.route.snapshot.paramMap.get('email');
		this.breadcrumbsService.setStep(0, 'Registration', this.router.url);

		this.registerForm = this.fb.group({
            password: ['', Validators.required],
            confirmPassword: ['', Validators.required]
        });
	}

	register(): void {
		if (this.registerForm.invalid) {
            this.registerForm.controls.password.markAsTouched();
            this.registerForm.controls.confirmPassword.markAsTouched();
            return;
        }
        if (this.registerForm.value.password !== this.registerForm.value.confirmPassword) {
            this.registerForm.controls.confirmPassword.setErrors({notEqual: true})
            return;
		}
		this.subscription.add(
            this.authenticationService.register(this.token, this.registerForm.value.password, this.type).subscribe(() => {
                this.subscription.add(
                    this.toolboxService.showObservableSuccess('Registration successful', 'Use your email and password to Log in').subscribe(() => {
                        this.router.navigate(['/login']);
                    })
                );
            })
		);
	}

	ngOnDestroy() {
        this.subscription.unsubscribe();
    }


}
