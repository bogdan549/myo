import { Component } from '@angular/core';
import { Router, Event, NavigationStart, NavigationEnd, NavigationError  } from '@angular/router';
import { Subscription } from 'rxjs';

import { AuthenticationService } from './_common/authentication.service';
import { ToolboxService } from './_common/toolbox.service';

@Component({
    selector: 'app',
    templateUrl: './app.component.html'
})
export class AppComponent {
    private subscription: Subscription = new Subscription();

    constructor(private router: Router,
        private authenticationService: AuthenticationService,
        private toolboxService: ToolboxService) {

        this.subscription.add(this.router.events.subscribe((event: Event) => {

            if (event instanceof NavigationStart) {
                // Show loading indicator
                if(event.url.search(/login/) === -1 &&
                    event.url.search(/register/) === -1 &&
                    event.url.search(/resetpassword?/) === -1 ){
                    this.authenticationService.checkAccess();
                    this.toolboxService.appUrl = event.url;
                }
            }

            if (event instanceof NavigationEnd) {
                // Hide loading indicator
            }

            if (event instanceof NavigationError) {
                // Hide loading indicator
                // Present error to user
            }

        }));

    }

    ngOnDestroy() {
		this.subscription.unsubscribe();
	}
}
