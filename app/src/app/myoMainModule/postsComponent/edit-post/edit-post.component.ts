import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Subscription } from 'rxjs';
import _ from 'lodash';

import { Story } from '../../_classes/story.class';
import { Media } from "../../_classes/content-media.class";
import { Category } from '../../_classes/category.class';
import { AddRecipientsToPostComponent } from '../add-recipients-to-post/add-recipients-to-post.component';
import { CategoryService } from "../../_services/category/category.service";
import { PostsService } from "../../_services/posts/posts.service";
import { SectionService } from "../../_services/section/section.service";
import { SectionSenior } from "../../_classes/senior.class";

@Component({
    selector: 'app-edit-post',
    templateUrl: './edit-post.component.html',
    styleUrls: ['./edit-post.component.css']
})
export class EditPostComponent implements OnInit {

    @Input() private post: Story;
    @Input() private sectionSeniors: SectionSenior[];
    @Output() onChangePost: EventEmitter<Story | null> = new EventEmitter();

    private subscription: Subscription = new Subscription();

    categories: Category[];
    editablePost: Story;

    // content: {link: string, mediaType: string}[] = [
    //     {
    //         link: 'https://www.gettyimages.co.uk/gi-resources/images/Embed/new/embed2.jpg',
    //         mediaType: 'image'
    //     },
    //     {
    //         link: 'https://media.koreus.com/201504/135-insolite-01.jpg',
    //         mediaType: 'image'
    //     },
    //     {
    //         link: 'https://www.youtube.com/embed/1t_sMynan_k',
    //         mediaType: 'video'
    //     }
    // ];

    content: Media[];

    shownMedia: {index: number, value:string, mediaType: string} = {index: 0, value:'', mediaType:'image'};

    constructor(
        private dialog: MatDialog,
        private categoryService: CategoryService,
        private postsService: PostsService
    ) { }

    ngOnInit() {
        this.editablePost = Object.assign({}, this.post);
        this.content = this.editablePost.content.media;
        this.fetchCategories();
        
        if(this.content.length){
            this.showImage(0);
        }
    }

    fetchCategories() {
        this.subscription.add(
            this.categoryService.getCategories().subscribe(categories => {
                this.categories = categories
            })
        );
    }

    acceptStoryEdit(event): void {
        event.stopPropagation();
        this.editablePost.status = 'ACCEPTED';
        this.subscription.add(
            this.postsService.acceptPost(this.editablePost.id).subscribe(() => {
                this.onChangePost.next(this.editablePost);
            })
        );
    }

    declineStoryEdit(event): void {
        event.stopPropagation();
        this.editablePost.status = 'DECLINED';
        this.subscription.add(
            this.postsService.declinePost(this.editablePost.id).subscribe(() => {
                this.onChangePost.next(this.editablePost);
            })
        );
    }

    removeReceipient(senior: SectionSenior): void {
        this.subscription.add(
            this.postsService.removeSenior(this.editablePost.id, senior.id).subscribe(r => {
                this.editablePost.recipients = this.editablePost.recipients.filter(el => el.id !== senior.id);
            })
        );
    }

    addRecepients(): void {
        const dialogRef = this.dialog.open(AddRecipientsToPostComponent, {
            width: '450px',
            data: {
                recipients: this.editablePost.recipients,
                availableRecipients: this.sectionSeniors
            },
        });

        this.subscription.add(
            dialogRef.afterClosed().subscribe(result => {
                if (result) {
                    /*var added: SectionSenior[] = _.differenceWith(result, before, (a, b) => a.id == b.id)
                    var removed: SectionSenior[] = _.differenceWith(before, result, (a, b) => a.id == b.id)*/

                    let seniorsIds = result.map(s => s.id);
                    this.subscription.add(
                        this.postsService.setSeniors(this.editablePost.id, seniorsIds).subscribe(r => {
                            this.editablePost.recipients = result;
                        })
                    );
                }
            })
        );
    }

    showImage(direction: number): void {
        if(direction > 0) {
            this.shownMedia.index = this.shownMedia.index%this.content.length;
            this.shownMedia.value = this.content[this.shownMedia.index].location;
            this.shownMedia.mediaType = this.content[this.shownMedia.index].content_type;
            this.shownMedia.index++;
        } else if(direction < 0) {
            this.shownMedia.index = this.shownMedia.index > 1 ? this.shownMedia.index : this.content.length+1;
            this.shownMedia.value = this.content[this.shownMedia.index-2].location;
            this.shownMedia.mediaType = this.content[this.shownMedia.index-2].content_type;
            this.shownMedia.index--;
        } else {
            this.shownMedia ={
                index: 1,
                value: this.content[0].location,
                mediaType: this.content[0].content_type
            }
        }
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
