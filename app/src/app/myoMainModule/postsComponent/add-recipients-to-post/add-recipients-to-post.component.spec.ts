import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRecipientsToPostComponent } from './add-recipients-to-post.component';

describe('AddRecipientsToPostComponent', () => {
  let component: AddRecipientsToPostComponent;
  let fixture: ComponentFixture<AddRecipientsToPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddRecipientsToPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRecipientsToPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
