import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

import { RECIPIENTS } from '../../../data.mock';
import { NewSenior } from "../../sectionsComponent/_classes/new-senior.class";
import { SectionSenior } from "../../_classes/senior.class";
import { Global } from '../../_services/global/global';

@Component({
    selector: 'app-add-recipients-to-post',
    templateUrl: './add-recipients-to-post.component.html',
    styleUrls: ['./add-recipients-to-post.component.css'],
    providers: [ Global ]
})
export class AddRecipientsToPostComponent implements OnInit {

    recipientsList: SectionSenior[];
    selectedRecipients: SectionSenior[];

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: {
            recipients: SectionSenior[],
            availableRecipients: SectionSenior[]
        },
        private global: Global
    ) {
        this.selectedRecipients = JSON.parse(JSON.stringify(this.data.recipients));
        this.recipientsList = JSON.parse(JSON.stringify(this.data.availableRecipients));
    }

    ngOnInit() {
    }

    isRecipientSelected(senior: SectionSenior): boolean {
        return this.selectedRecipients.filter(e => e.id == senior.id).length > 0;
    }

    toggleRecipient(senior: SectionSenior): void {
        var isRecipientSelected = this.isRecipientSelected(senior)
        if (isRecipientSelected) {
            this.selectedRecipients = this.selectedRecipients.filter(e => e.id != senior.id);
        } else {
            this.selectedRecipients.push(senior);
        }
    }

}
