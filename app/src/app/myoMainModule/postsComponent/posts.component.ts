import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { Story } from '../_classes/story.class';
import { Section } from '../_classes/section.calss';
import { BreadcrumbsService } from '../../breadcrumbsComponent/services/breadcrumbs.service';
import { PostsService } from '../_services/posts/posts.service';
import { SectionService } from '../_services/section/section.service';
import { FacilityService } from "../_services/facility/facility.service";
import { Facility } from "../_classes/facility.class";
import { CategoryService } from "../_services/category/category.service";
import { Category } from "../_classes/category.class";

import _ from "lodash";
import { SectionSenior } from "../_classes/senior.class";
import { SeniorService } from "../_services/senior/senior.service";

@Component({
    selector: 'app-posts',
    templateUrl: './posts.component.html',
    styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

    private subscription: Subscription = new Subscription();
    editedStory: Story;
    stories: Story[] = [];
    facilities: Facility[];
    sections: Section[];
    sectionSeniors: SectionSenior[] = [];

    categories: Category[];
    categoriesById: {};   // map id->Category of categories

    selectedFacilityId: number;
    selectedSectionId: number;

    sortKey: string = ''

    constructor(private router: Router,
        private breadcrumbsService: BreadcrumbsService,
        private postsService: PostsService,
        private facilityService: FacilityService,
        private sectionService: SectionService,
        private categoryService: CategoryService,
        private seniorService: SeniorService,
    ) { }

    ngOnInit() {
        this.breadcrumbsService.setStep(1, 'Posts', this.router.url);
        this.getFacilities();
        this.fetchCategories();
    }

    fetchCategories() {
        this.subscription.add(
            this.categoryService.getCategories().subscribe(categories => {
                this.categories = categories
                this.categoriesById = categories.reduce((obj, item) => {
                    obj[item.id] = item;
                    return obj;
                }, {})
            })
        );
    }

    getStories(sectionId: number): void {
        this.subscription.add(
            this.postsService.getPostListForSection(sectionId).subscribe(posts => {
                this.stories = posts;
                //this.stories.forEach(el => {
                  //  el.recipients.push(el.recipients[0], el.recipients[0], el.recipients[0], el.recipients[0]); // temp solution
                //})
                
                this.sortBy('posted_on');
            })
        );
    }

    getFacilities(): void {
        this.subscription.add(
            this.facilityService.getFacilityList().subscribe(facilities => {
                this.facilities = facilities;
                if (facilities.length > 0) {
                    let firstfacilityId = facilities[0].id;
                    this.selectedFacilityId = firstfacilityId;
                    this.facilityChanged(firstfacilityId)
                }
                console.log(this.facilities);
            })
        );
    }

    getSections(facilityId: number): void {
        this.subscription.add(
            this.sectionService.getFacilitySections(facilityId).subscribe(sections => {
                this.sections = sections;

                if (sections.length > 0) {
                    let firstSectionId = sections[0].id;
                    this.selectedSectionId = firstSectionId;
                    this.getStories(firstSectionId);

                    this.subscription.add(
                        this.seniorService.getSectionSeniorList(firstSectionId).subscribe(seniors => {
                            this.sectionSeniors = seniors;
                        })
                    );
                } else {
                    this.stories = [];
                }

                console.log(this.sections);
            })
        );
    }

    facilityChanged(facilityId: number): void {
        console.log("facility changed: " + facilityId);
        this.getSections(facilityId)
    }

    sectionChanged(sectionId: number): void {
        console.log("section changed: " + sectionId);
        this.getStories(sectionId)
    }

    editSory(story: Story): void {
        this.editedStory = Object.assign({}, story);
    }

    changePost(story: Story): void {
        this.editedStory = null;
        if (!story) return;
        this.subscription.add(
            this.postsService.updatePost(story).subscribe(result => {
                const storyIndex: number = this.stories.indexOf(this.stories.find(el => el.id === story.id));
                this.stories[storyIndex] = story;
            })
        );
    }

    sortBy(orderKey: string): void {
        console.log(this.sortKey === orderKey);
        this.stories = _.orderBy(this.stories, orderKey.split(','), this.sortKey === orderKey ? 'asc' : 'desc');
        this.sortKey = this.sortKey === orderKey ? orderKey+'-' : orderKey;
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
