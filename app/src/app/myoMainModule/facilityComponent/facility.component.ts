import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import _ from 'lodash';

import { Facility } from '../_classes/facility.class';
import { Section } from '../_classes/section.calss';
import { BreadcrumbsService } from '../../breadcrumbsComponent/services/breadcrumbs.service';
import { FacilityService } from '../_services/facility/facility.service';
import { SectionService } from '../_services/section/section.service';
import { DataService } from '../_services/data/data.service';

@Component({
    selector: 'app-facility',
    templateUrl: './facility.component.html',
    styleUrls: ['./facility.component.css']
})
export class FacilityComponent implements OnInit {

    private subscription: Subscription = new Subscription();
    facilityId: number;
    facility: Facility;
    sectionList: Section[];

    sortKey: string = ''

    newSectionForm: FormGroup = new FormGroup({
        title: new FormControl()
    });

    constructor(private route: ActivatedRoute,
        private router: Router,
        private breadcrumbsService: BreadcrumbsService,
        private facilityService: FacilityService,
        private fb: FormBuilder,
        private sectionService: SectionService,
        private dataService: DataService) { }


    ngOnInit() {
        this.facilityId = +this.route.snapshot.paramMap.get('facilityId');
        this.getFacility(this.facilityId);
        this.getFacilitySections();
        this.newSectionForm = this.fb.group({
            title: ['', Validators.required]
        });
    }

    getFacilitySections(): void {
        this.subscription.add(
            this.sectionService.getFacilitySections(this.facilityId).subscribe(sectionList => {
                console.log(sectionList);
                this.sectionList = sectionList;
				this.dataService.setSections(sectionList);
            }, error => {
                console.log(error);
            })
        );
    }

    getFacility(facilityId: number): void {
        this.subscription.add(
            this.dataService.getFacility(facilityId).subscribe(facility => {
                this.facility = facility;
                this.breadcrumbsService.setStep(1, this.facility.name, this.router.url);
            }, error => {
                console.log(error);
            })
        );
    }

    addSection(): void {
        if (this.newSectionForm.valid) {
            this.subscription.add(
                this.sectionService.addSection(this.facilityId, this.newSectionForm.value.title).subscribe(() => {
                    this.getFacilitySections();
                    this.newSectionForm.setValue({ 'title': '' });
                    this.newSectionForm.controls.title.markAsUntouched();
                }, error => {
                    console.log(error);
                })
            );
        } else {
            Object.keys(this.newSectionForm.controls).forEach(field => {
                const control = this.newSectionForm.get(field);
                control.markAsTouched({ onlySelf: true });
            });
        }
    }

    sortBy(orderKey: string): void {
      console.log(this.sortKey === orderKey);
      this.sectionList = _.orderBy(this.sectionList, orderKey.split(','), this.sortKey === orderKey ? 'asc' : 'desc');
      this.sortKey = this.sortKey === orderKey ? '' : orderKey;
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
