import { Injectable } from '@angular/core';
import { Observable, of, Subject, BehaviorSubject } from 'rxjs';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../../environments/environment";
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild, CanDeactivate } from '@angular/router';

import { User } from '../../_classes/user.class';
import { ToolboxService } from '../../../_common/toolbox.service';
import { AuthenticationService } from '../../../_common/authentication.service';
import { AdminsComponent } from '../../adminsComponent/admins.component';
import {CompanyService} from "../company/company.service";

interface CanComponentDeactivate {
    canDeactivate: () => Observable<boolean> | boolean;
}

@Injectable({
    providedIn: 'root'
})
export class UserService implements CanActivateChild, CanDeactivate<CanComponentDeactivate> {

    private _user: User;
    private userSubject: Subject<User> = new Subject();
    private requestFalg: boolean = false;
    private _rootLink: string = '';
    rootLinkSubject: Subject<string> = new BehaviorSubject(this._rootLink);

    constructor(private http: HttpClient,
                private toolboxService: ToolboxService,
                private authenticationService: AuthenticationService,
                private companyService: CompanyService,
    ) { }


    set user(user: User) {
        this._user = user;
        this.setRole(this._user.user_role);
    }

    get user(): User {
      return this._user;
    }

    canDeactivate(component: AdminsComponent): Observable<boolean> | boolean {
        return component.canDeactivate ? component.canDeactivate() : true;
    }

    setRootLink(link: string): void {
        this.rootLinkSubject.next(link);
    }

    getRootLink(): Observable<string> {
        return this.rootLinkSubject.asObservable();
    }

    getUser(): Observable<User> {
        if (this._user) {
            return of(this._user);
        }
        if(!this.requestFalg){
            this.requestFalg = true;
            this.requestUser();
        }
        return this.userSubject.asObservable();

    }

    getRole(): string {
        let role = localStorage.getItem('myo_user_role')
        if(!role) {
            this.authenticationService.logout();
            return null;
        } else {
            return this.toolboxService.base64Decode(role);
        }
    }

    setRole(role: string): void {
        localStorage.setItem('myo_user_role', this.toolboxService.base64Encode(role));
    }

    normalizeRole(role: string): string {
      return role.toLocaleLowerCase().split("_").map(word => word[0].toLocaleUpperCase() + word.substr(1, word.length)).join(" ");
    }

    private requestUser(): void {
        this.http.get<User>(environment.apiUrl + `user/me`).subscribe(user => {

            if(user.user_role.toUpperCase() == 'TECHNICAL_SUPPORT' && !user.company_id){
              this.companyService.getCompanyList().subscribe(companies => {
                if(companies.length > 0){
                  user.company_id = companies[0].id;
                  user.company_name = companies[0].name;
                  this.userSubject.next(user);
                } else {
                  console.log("User has no comapnies");
                }
              })
            } else {
              this.userSubject.next(user);
            }
        });
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        return this.checkAccess(route.routeConfig.path);
    }

    private checkAccess(url: string): boolean {
        const role = this.getRole();
        if(!role){
            return false;
        }
        switch(role.toUpperCase()){
            case 'COMPANY_HEAD':
            case 'TECHNICAL_SUPPORT':
            return true;
            case 'FACILITY_HEAD':
            if(url === 'facility'){
                return false;
            }
            return true;
            case 'SECTION_HEAD':
            if(url === 'facility' || url === 'facility/:facilityId'){
                return false;
            }
            return true;

        }
    }

}
