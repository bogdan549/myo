import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';

// import { EMPLOYEES } from '../../../data.mock';
import { Employee, EmployeeRole } from '../../_classes/employee.class';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../../environments/environment";
import { NewEmployee } from '../../employeesComponent/add-employee-dialog/_classes/new-employee.class';

@Injectable({
    providedIn: 'root'
})
export class EmployeeService {

    constructor(private http: HttpClient) { }

    getEmployeeListForFacilityId(facilityId: number): Observable<Employee[]> {
        return this.http.get<Employee[]>(environment.apiUrl + `facility/${facilityId}/employees`);
    }

    getEmployee(employeeId: number, role: string): Observable<Employee> {
        return this.http.get<Employee>(environment.apiUrl + `user/employee/${employeeId}/${role}`);
    }

    addEmployee(employee: NewEmployee): Observable<Employee> {
        return this.http.post<Employee>(environment.apiUrl + `user/employee`, employee);
    }

    editEmployee(employee, id:number): Observable<any> {
        return this.http.patch(environment.apiUrl + `user/employee/${id}`, employee);
    }

    deleteEmployee(id:number): Observable<any> {
        return this.http.delete(environment.apiUrl + `user/employee/${id}`);
    }

    getEmploeesRolesList(): { key: string, value: string }[] {
        let rolesList: { key: string, value: string }[] = [];
        Object.keys(EmployeeRole).forEach(key => {
            rolesList.push({ key: key, value: EmployeeRole[key].toLowerCase().replace('_', ' ') })
        });
        return rolesList;
    }

    reinviteEmployee(employeeEmail: string): Observable<any> {
        return this.http.post<Employee[]>(environment.apiUrl + `user/employee/${encodeURIComponent(employeeEmail)}/reinvite/`, {});
    }
}
