import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

// import { FACILITIES } from '../../../data.mock';
import { Facility } from '../../_classes/facility.class';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../../environments/environment";
import {Category} from "../../_classes/category.class";

@Injectable({
    providedIn: 'root'
})
export class CategoryService {

    constructor(private http: HttpClient) { }

    getCategories(): Observable<Category[]> {
        return this.http.get<Facility[]>(environment.apiUrl + 'category/list');
    }

}
