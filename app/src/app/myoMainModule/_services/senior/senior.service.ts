import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from "@angular/common/http";
import { CanDeactivate }  from '@angular/router';

import { SectionSenior } from '../../_classes/senior.class';
import { SeniorProfile } from '../../_classes/senior-profile.class';
import { Relative } from '../../_classes/relative.class';
import { NewSenior } from '../../sectionsComponent/_classes/new-senior.class';
import { environment } from "../../../../environments/environment";
import { EditSenior } from '../../seniorComponent/_classes/edit-senior.class';
import { SeniorComponent } from '../../seniorComponent/senior.component';

interface CanComponentDeactivate {
    canDeactivate: () => Observable<boolean> | boolean;
}

@Injectable({
    providedIn: 'root'
})
export class SeniorService implements CanDeactivate<CanComponentDeactivate> {

    constructor(private http: HttpClient) { }

    canDeactivate(component: SeniorComponent): Observable<boolean> | boolean {
        return component.canDeactivate ? component.canDeactivate() : true;
    }

    getSenior(seniorId: number): Observable<SeniorProfile> {
        return this.http.get<SeniorProfile>(environment.apiUrl + `senior/${seniorId}`);
    }

    getSectionSeniorList(sectionId: number): Observable<SectionSenior[]> {
        return this.http.get<SectionSenior[]>(environment.apiUrl + `senior/section/${sectionId}`);
    }

    getSeniorRelatives(seniorId: number): Observable<Relative[]> {
        return this.http.get<Relative[]>(environment.apiUrl + `senior/${seniorId}/relatives`);
    }

    addSenior(senior: NewSenior): Observable<any> {
        return this.http.put(environment.apiUrl + `senior/`, senior);
    }

    updateSenior(senior: EditSenior, seniorId: number): Observable<any> {
        return this.http.post(environment.apiUrl + `senior/${seniorId}`, senior);
    }

    deleteSenior(seniorId: number): Observable<any> {
        return this.http.delete(environment.apiUrl + `senior/${seniorId}`);
    }

    removeRelative(seniorId: number, relativeId: number): Observable<any> {
        return this.http.delete(environment.apiUrl + `senior/${seniorId}/unrelate/${relativeId}`);
    }

    reinviteRelative(relativeEmail: string): Observable<any> {
        return this.http.post<Relative[]>(environment.apiUrl + `relative/${encodeURIComponent(relativeEmail)}/reinvite/`, {});
    }
}
