import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient } from "@angular/common/http";

// import { FACILITIES } from '../../../data.mock';
import { Section } from '../../_classes/section.calss';
import { environment } from "../../../../environments/environment";

@Injectable({
    providedIn: 'root'
})
export class SectionService {

    constructor(private http: HttpClient) { }

    getFacilitySections(facilityId: number): Observable<Section[]> {
        return this.http.get<Section[]>(environment.apiUrl + `section/facility/${facilityId}`);  
    }

    getSection( sectionId: number): Observable<Section>{
        return this.http.get<Section>(environment.apiUrl + `section/${sectionId}`);  
    }

    addSection(facilityId: number, sectionName: string): Observable<any> {
        return this.http.post(environment.apiUrl + `section/${facilityId}/${sectionName}`, {});         
    }

}
