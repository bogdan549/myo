import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

// import { POSTS } from '../../../data.mock';
import { Story } from '../../_classes/story.class';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(private http: HttpClient) { }

  getPostListForSection(sectionId: number): Observable<Story[]> {
    return this.http.get<Story[]>(environment.apiUrl + `story/section/${sectionId}`);
    //return of(POSTS);
  }

  acceptPost(postId: number): Observable<any> {
    return this.http.get<any>(environment.apiUrl + `story/${postId}/accept`);
  }

  declinePost(postId: number): Observable<any> {
    return this.http.get<any>(environment.apiUrl + `story/${postId}/decline`);
  }

  updatePost(story: Story): Observable<any> {
    return this.http.post<any>(environment.apiUrl + `story/${story.id}/update/${story.category}`, story.content.description);
  }

  removeSenior(postId: number, seniorId: Number): Observable<any> {
    return this.http.delete<any>(environment.apiUrl + `story/${postId}/remove/senior/${seniorId}`);
  }

  setSeniors(postId: number, seniorId: Number[]): Observable<any> {
    return this.http.post<any>(environment.apiUrl + `story/${postId}/set/seniors`, seniorId);
  }

}
