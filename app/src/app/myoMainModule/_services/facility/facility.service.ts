import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { HttpClient } from "@angular/common/http";

import { Facility } from '../../_classes/facility.class';
import { environment } from "../../../../environments/environment";
import { UserService } from '../user/user.service';
import { take } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class FacilityService {

    private facilitySubject: Subject<Facility[]> = new Subject();

    constructor(private http: HttpClient,
        private userService: UserService) { }

    getFacilityList(): Observable<Facility[]> {
        this.userService.getUser().pipe(take(1)).subscribe(user => {
            this.http.get<Facility[]>(environment.apiUrl + `facility/company/${user.company_id}`).subscribe(facilityList =>{
                let facilities = facilityList;
                if (user.user_role.toUpperCase() !== 'COMPANY_HEAD' && user.user_role.toUpperCase() !== 'TECHNICAL_SUPPORT'){
                    facilities = facilities.filter(el => el.id === user.facility_id);
                }
                this.facilitySubject.next(facilities);
            })
        })
        return this.facilitySubject.asObservable();
    }

    getFacility(facilityId: number): Observable<Facility> {
        return this.http.get<Facility>(environment.apiUrl + `facility/${facilityId}`);
    }

    addFacility(companyId:number, facilityName: string): Observable<Facility> {
        return this.http.post<Facility>(environment.apiUrl + `facility/${companyId}/${facilityName}`, {});
    }
}
