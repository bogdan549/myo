import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from "@angular/common/http";

import { environment } from "../../../../environments/environment";
import { Relation } from '../../_classes/relations.class';

@Injectable({
  providedIn: 'root'
})
export class StaticService {

  constructor(private http: HttpClient) { }

  getRelationList(): Observable<Relation[]> {
    return this.http.get<Relation[]>(environment.apiUrl + `relationship/list`);
  }
}
