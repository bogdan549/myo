import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../../environments/environment";

import { Admin } from '../../_classes/admin.class';

@Injectable({
    providedIn: 'root'
})
export class AdminsService {

    constructor(private http: HttpClient) { }

    getAdmins(): Observable<Admin[]> {
        return this.http.get<Admin[]>(environment.apiUrl + `user/employee/admins`);
    }

    // reinviteAdmin(employeeEmail: string): Observable<any> {
    //     return this.http.get<Admin[]>(environment.apiUrl + `user/employee/${encodeURIComponent(employeeEmail)}/reinvite`);
    // }
}
