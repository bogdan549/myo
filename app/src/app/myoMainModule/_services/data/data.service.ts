import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { Company } from '../../_classes/company.class';
import { Facility } from '../../_classes/facility.class';
import { Section } from '../../_classes/section.calss';
import { FacilityService } from '../facility/facility.service';
import { SectionService } from '../section/section.service';

@Injectable({
    providedIn: 'root'
})
export class DataService {

    constructor(private facilityService: FacilityService,
        private sectionService: SectionService) { }

    private _company: Company;
    private _facilities: Facility[];
    private _sections: Section[];

    getCompany(id: number): Company {
        return this._company
    }
    setCompany(company: Company) {
        this._company = company;
    }

    getFacility(id: number): Observable<Facility> {
        if (this._facilities) {
            return of(this._facilities.find(el => el.id === id));
        } else {
            return this.facilityService.getFacility(id);
        }
    }
    setFacilities(facilities: Facility[]) {
        this._facilities = facilities;
    }

    getSection(id: number): Observable<Section> {
        if (this._sections) {
            return of(this._sections.find(el => el.id === id));
        } else {
            return this.sectionService.getSection(id);
        }
    }
    setSections(sections: Section[]) {
        this._sections = sections;
    }

}
