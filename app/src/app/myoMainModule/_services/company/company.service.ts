import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from "@angular/common/http";

import { Company } from '../../_classes/company.class';
import { environment } from "../../../../environments/environment";
import { CreateCompanyRequest } from "../../../createCompanyComponent/classes/createCompanyRequest.class";

@Injectable({
    providedIn: 'root'
})
export class CompanyService {

    constructor(private http: HttpClient) { }

    getCompanyList(): Observable<Company[]> {
        return this.http.get<Company[]>(environment.apiUrl + 'company/list');
    }

    createCompany(companyRequest: CreateCompanyRequest): Observable<Company> {
      return this.http.put<Company>(environment.apiUrl + `company/create`, companyRequest);
    }

    uploadLogo(companyId: number, logoFileData: any): Observable<any> {
      return this.http.post<any>(environment.apiUrl + `company/${companyId}/image`, logoFileData);
    }

}
