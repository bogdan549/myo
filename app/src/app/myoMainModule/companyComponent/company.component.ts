import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import _ from 'lodash';

import { BreadcrumbsService } from '../../breadcrumbsComponent/services/breadcrumbs.service';
import { FacilityService } from '../_services/facility/facility.service';
import { AuthenticationService } from '../../_common/authentication.service';
import { DataService } from '../_services/data/data.service';
import { Facility } from '../_classes/facility.class';
import { UserService } from '../_services/user/user.service';
import { Global } from '../_services/global/global';

@Component({
	selector: 'app-company',
	templateUrl: './company.component.html',
	styleUrls: ['./company.component.css'],
	providers: [ Global ]
})

export class CompanyComponent implements OnInit {

	private subscription: Subscription = new Subscription();
	facilities: Facility[];
	companyId: number;
	sortKey: string = ''
	newFacilityForm: FormGroup = new FormGroup({
        title: new FormControl()
    });

	constructor(private router: Router,
		private breadcrumbsService: BreadcrumbsService,
		private facilityService: FacilityService,
		private fb: FormBuilder,
		private authenticationService: AuthenticationService,
		private dataService: DataService,
		private userService: UserService,
		private global: Global) { }

	ngOnInit() {
		this.getFacilities();
		this.getUser();
		this.newFacilityForm = this.fb.group({
            title: ['', Validators.required]
        });
	}

	getUser(): void {
		this.subscription.add(
			this.userService.getUser().subscribe(user => {
				this.companyId = user.company_id;
				this.breadcrumbsService.setStep(0, user.company_name+' Overview', this.router.url);
				this.getFacilities();
			}, error => {
				console.log(error);
			})
		);
	}

//	getCompany():void {
//		this.subscription.add(
//			this.companyService.getCompanyList().subscribe(companyList => {
//				this.company = companyList[0];
//				this.dataService.setCompany(this.company);
//				this.breadcrumbsService.setStep(0, this.company.name+' Overview', this.router.url);
//				this.getFacilities();
//			}, error => {
//				console.log(error);
//			})
//		);
//	}

	getFacilities(): void {
		this.subscription.add(
			this.facilityService.getFacilityList().subscribe(facilities => {
				console.log(facilities);
				this.facilities = facilities;
				this.dataService.setFacilities(facilities);
			}, error => {
				console.log(error);
				if(error.status === 401) {
					this.authenticationService.logout();
                }
			})
		);
	}

	addFacility(): void {
		if(this.newFacilityForm.valid){
			this.subscription.add(
				this.facilityService.addFacility(this.companyId, this.newFacilityForm.value.title).subscribe(() => {
					this.getFacilities();
					this.newFacilityForm.setValue({'title': ''});
					this.newFacilityForm.controls.title.markAsUntouched();

				}, error => {
					console.log(error);
				})
			);
		} else {
			Object.keys(this.newFacilityForm.controls).forEach(field => {
				const control = this.newFacilityForm.get(field);
				control.markAsTouched({ onlySelf: true });
			});
		}
	}

	sortBy(orderKey: string): void {
		console.log(this.sortKey === orderKey);
    this.facilities = _.orderBy(this.facilities, orderKey, this.sortKey === orderKey ? 'asc' : 'desc');
    this.sortKey = this.sortKey === orderKey ? orderKey+'-' : orderKey;
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}

}


