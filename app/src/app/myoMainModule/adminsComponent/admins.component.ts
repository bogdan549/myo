import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import _ from 'lodash';

import { Admin } from '../_classes/admin.class';
import { User } from '../_classes/user.class';
import { BreadcrumbsService } from '../../breadcrumbsComponent/services/breadcrumbs.service';
import { AdminsService } from '../_services/admins/admins.service';
import { UserService } from '../_services/user/user.service';
import { EmployeeService } from '../_services/employee/employee.service';



@Component({
    selector: 'app-admins',
    templateUrl: './admins.component.html',
    styleUrls: ['./admins.component.css']
})
export class AdminsComponent implements OnInit {

    private subscription: Subscription = new Subscription();
    admins: Admin[];
    user: User;

    sortKey: string = ''

    constructor(private router: Router,
        private breadcrumbsService: BreadcrumbsService,
        private adminsService: AdminsService,
        private employeeService: EmployeeService,
        private userService: UserService) { }

    ngOnInit() {
        this.breadcrumbsService.setStep(1, 'Admin', this.router.url);
        this.getAdminList();
        this.getUser();
    }

    getUser(): void {
        this.user = this.userService.user;
        if(!this.user){
            this.subscription.add(
                this.userService.getUser().subscribe(user => {
                    this.user = user;
                })
            );
        }
    }

    getAdminList(): void {
        this.subscription.add(
            this.adminsService.getAdmins().subscribe(admins => {
                this.admins = admins;                
            })
        );

    }

    reinviteAdmin(employeeEmail: string): void {
      console.log(employeeEmail);
        this.subscription.add(
            this.employeeService.reinviteEmployee(employeeEmail).subscribe(() => {
                this.getAdminList();
            })
        );
    }

    sortBy(orderKey: string): void {
        console.log(this.sortKey === orderKey);
        this.admins = _.orderBy(this.admins, orderKey.split(','), this.sortKey === orderKey ? 'asc' : 'desc');
        this.sortKey = this.sortKey === orderKey ? '' : orderKey;
    }

    canDeactivate(): Observable<boolean> | boolean {
        return this.user.user_role.toUpperCase() != 'CAREGIVER';
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }


}
