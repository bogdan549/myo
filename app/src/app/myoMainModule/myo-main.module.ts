import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { MatDialogModule, MatStepperModule, MatProgressSpinnerModule, MatFormFieldModule, MatNativeDateModule, MatInputModule } from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';

import { CompanyComponent } from './companyComponent/company.component';
import { MyoMainComponent } from './myo-main.component';
import { SideBarComponent } from './sideBarComponent/side-bar/side-bar.component';
import { SectionComponent } from './sectionsComponent/section.component';
import { FacilityComponent } from './facilityComponent/facility.component';
import { EmployeesComponent } from './employeesComponent/employees.component';
import { EmployeeComponent } from './employeeComponent/employee.component';
import { SeniorComponent } from './seniorComponent/senior.component';
import { PostsComponent } from './postsComponent/posts.component';
import { AddSeniorDialogComponent } from './sectionsComponent/add-senior-dialog/add-senior-dialog.component';
import { AddEmployeeDialogComponent } from './employeesComponent/add-employee-dialog/add-employee-dialog.component';
import { EditPostComponent } from './postsComponent/edit-post/edit-post.component';
import { AddRecipientsToPostComponent } from './postsComponent/add-recipients-to-post/add-recipients-to-post.component';
import { AdminsComponent } from './adminsComponent/admins.component';
import { RelativesComponent } from './seniorComponent/relatives/relatives.component';
import { TagInputModule } from 'ngx-chips';
import { SafePipe } from './_pipes/safe.pipe';
import { DatePipe } from '@angular/common';

@NgModule({
    imports:      [
        BrowserModule,
        FormsModule, ReactiveFormsModule,
        HttpClientModule,
        RouterModule,
        MatDialogModule, MatStepperModule, MatProgressSpinnerModule,
        TagInputModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatNativeDateModule, MatInputModule
    ],
    declarations: [
        CompanyComponent,
        MyoMainComponent,
        SideBarComponent,
        SectionComponent,
        FacilityComponent,
        EmployeesComponent,
        EmployeeComponent,
        SeniorComponent,
        PostsComponent,
        AddSeniorDialogComponent,
        AddEmployeeDialogComponent,
        EditPostComponent,
        AddRecipientsToPostComponent,
        AdminsComponent,
        RelativesComponent,
        SafePipe,
        // DatePipe
     ],
    exports: [CompanyComponent, MyoMainComponent, SectionComponent, FacilityComponent, AdminsComponent],
    entryComponents: [AddSeniorDialogComponent, AddEmployeeDialogComponent, AddRecipientsToPostComponent],
    providers: [DatePipe]
})
export class MyoMainModule { }
