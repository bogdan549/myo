import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MatDialogRef } from '@angular/material';
import { MatStepper } from '@angular/material';
import { DatePipe } from '@angular/common';

import { Employee, EmployeeRole } from '../../_classes/employee.class';
import { Facility } from '../../_classes/facility.class';
import { Section } from '../../_classes/section.calss';
import { NewEmployee } from './_classes/new-employee.class';
import { FacilityService } from '../../_services/facility/facility.service';
import { SectionService } from '../../_services/section/section.service';
import { EmployeeService } from '../../_services/employee/employee.service';
import { ToolboxService } from '../../../_common/toolbox.service';
import {CompanyService} from "../../_services/company/company.service";
import {Company} from "../../_classes/company.class";


@Component({
    selector: 'app-add-employee-dialog',
    templateUrl: './add-employee-dialog.component.html',
    styleUrls: ['./add-employee-dialog.component.css']
})
export class AddEmployeeDialogComponent implements OnInit {

    @ViewChild('stepper') stepper: MatStepper;

    private subscription: Subscription = new Subscription();
    private defaulEmployeeObj: NewEmployee;
    employeeRoles: { key: string, value: string }[] = this.employeeService.getEmploeesRolesList();
    facilityList: Facility[];
    company: Company;
    sectionList: Section[];
    selectedFacilityId: number;
    selectedSectionId: number;
    facilitySelectDisabled: boolean = true;
    sectionSelectDisabled: boolean = true;
    sectionHidden: boolean = false;

    newEmployee: FormGroup = new FormGroup({
        email: new FormControl(),
        password: new FormControl(),
        type: new FormControl(),
        postId: new FormControl(),
        firstName: new FormControl(),
        lastName: new FormControl(),
        // facility: new FormControl(),
        previousHobbies: new FormControl(),
        hobbies: new FormControl(),
        motivation: new FormControl(),
        placeOfBirth: new FormGroup({
            city: new FormControl(),
            country: new FormControl()
        }),
        dateOfBirth: new FormControl(),
        phoneNumber: new FormControl(),
        startYear: new FormControl()
    });

    constructor(private fb: FormBuilder,
        private facilityService: FacilityService,
        private sectionService: SectionService,
        private companyService: CompanyService,
        private dialogRef: MatDialogRef<AddEmployeeDialogComponent>,
        private employeeService: EmployeeService,
        private datePipe: DatePipe,
        private toolboxService: ToolboxService) {
            dialogRef.disableClose = true;
        }

    ngOnInit() {
        this.getFacilityList();
        this.getCompanies();
        console.log(this.employeeRoles)
        this.newEmployee = this.fb.group({
            email: ['', Validators.required],
            password: [''+(new Date()).getTime(), Validators.required],
            type: ['', Validators.required],
            postId: ['', Validators.required],
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            // facility: ['', Validators.required],
            previousHobbies: [''],
            hobbies: [''],
            motivation: [''],
            placeOfBirth: this.fb.group({
                city: [''],
                country: ['']
            }),
            dateOfBirth: [this.datePipe.transform(Date.now(), 'yyyy-MM-dd')],
            phoneNumber: [''],
            startYear: [''],
        });

        this.defaulEmployeeObj = this.newEmployee.value;
        this.subscription.add(
            this.dialogRef.backdropClick().subscribe(() => {
                this.tryToCloseDialog();
            })
        );
    }

    tryToCloseDialog(): void {
        if(this.checkForEmployeeChange()){
            const header: string = 'Information not saved yet';
            const text: string = 'You haven’t saved the changed you have made. If you proceed now your changes won’t be saved.';
            this.subscription.add(
                this.toolboxService.confirm(header, text).subscribe(res => {
                    if (res) {
                        this.dialogRef.close();
                    }
                })
            );
        } else {
            this.dialogRef.close();
        }
    }

    checkForEmployeeChange(): boolean {
        for (let key of Object.keys(this.defaulEmployeeObj)) {
            if (key === 'placeOfBirth') continue;
            if (this.newEmployee.value[key] !== this.defaulEmployeeObj[key]) {
                console.log(key);
                return true;
            }
        }
        console.log('NOTchanged');
        return false
    }

    getFacilityList(): void {
        this.subscription.add(
            this.facilityService.getFacilityList().subscribe(facilityList => {
                this.facilityList = facilityList;
            })
        );
    }

    getCompanies(): void {
      this.subscription.add(
        this.companyService.getCompanyList().subscribe(companies => {
          this.company = companies[0];
        })
      );
    }

    getSections(facilityId: number): void {
        this.sectionService.getFacilitySections(facilityId).subscribe(sections => {
            this.sectionList = sections;
        });
    }

    facilityChanged(facilityId: number): void {
        console.log("facility changed: " + facilityId);
        switch (this.newEmployee.value.type) {
            case 'SECTION_HEAD':
            case 'CAREGIVER':
                this.sectionSelectDisabled = false;
                this.getSections(facilityId)
                break;
        }
    }

    checkFirstStep(stepper): void {
        if (this.newEmployee.controls.type.valid) {
            switch (this.newEmployee.value.type) {
                case 'COMPANY_HEAD':
                case 'TECHNICAL_SUPPORT':
                  this.newEmployee.controls.postId.setValue(+this.company.id);
                  stepper.next();
                  break;
                case 'FACILITY_HEAD':
                  this.sectionHidden = true;
                  this.facilitySelectDisabled = false;
                  break;
                case 'SECTION_HEAD':
                case 'CAREGIVER':
                    this.facilitySelectDisabled = false;
                    break;
            }
        }
    }

    checkSecondStep(): void {

        if (this.newEmployee.controls.type.valid) {
            switch (this.newEmployee.value.type) {
                case 'COMPANY_HEAD':
                case 'TECHNICAL_SUPPORT':
                case 'FACILITY_HEAD':
                    if(this.selectedFacilityId){
                        this.newEmployee.controls.postId.setValue(+this.selectedFacilityId);
                    }
                    break;
                case 'SECTION_HEAD':
                case 'CAREGIVER':
                    if(this.selectedSectionId){
                        this.newEmployee.controls.postId.setValue(+this.selectedSectionId);
                    }
                    break;
            }
        }
        if (this.newEmployee.controls.postId.valid) {
            this.stepper.next();
        }
    }

    checkStep(keysArr: string[]): void {
        for (let i = 0; i < keysArr.length; i++) {
            if (keysArr[i] === 'placeOfBirth') {
                this.newEmployee.get('placeOfBirth').get('city').markAsTouched();
                this.newEmployee.get('placeOfBirth').get('country').markAsTouched();
            }
            this.newEmployee.controls[keysArr[i]].setValue(this.newEmployee.controls[keysArr[i]].value.trim());
            this.newEmployee.controls[keysArr[i]].markAsTouched();
        }
    }

    finishStep(keysArr: string[]): void {
        this.checkStep(keysArr);
        console.log(this.newEmployee.value);
        if (this.newEmployee.invalid) return;
        delete this.newEmployee.value.section;
        delete this.newEmployee.value.facility;

        let newEmployeeObj: NewEmployee = this.newEmployee.value;
        this.dialogRef.close(newEmployeeObj);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
