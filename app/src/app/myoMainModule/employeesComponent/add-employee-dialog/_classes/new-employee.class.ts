import { EmployeeRole } from "../../../_classes/employee.class";

export class NewEmployee {
    email: string;
    password: string;
    type: EmployeeRole;
    postId: number;
    firstName: string;
    lastName: string;
    previousHobbies: string;
    hobbies: string;
    motivation: string;
    placeOfBirth: {
      city: string,
      country: string
    };
    dateOfBirth: string;
    phoneNumber: string;
    startYear: number;
    constructor () {
        
    }
}