import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { Subscription } from 'rxjs';
import _ from 'lodash';

import { Employee, EmployeeRole } from "../_classes/employee.class";
import { BreadcrumbsService } from '../../breadcrumbsComponent/services/breadcrumbs.service';
import { ToolboxService } from '../../_common/toolbox.service';
import { AddEmployeeDialogComponent } from './add-employee-dialog/add-employee-dialog.component';
import { EmployeeService } from '../_services/employee/employee.service';
import { Facility } from "../_classes/facility.class";
import { FacilityService } from "../_services/facility/facility.service";
import { UserService } from '../_services/user/user.service';
import { Global } from '../_services/global/global';

@Component({
    selector: 'app-employees',
    templateUrl: './employees.component.html',
    styleUrls: ['./employees.component.css'],
    providers: [ Global ]
})
export class EmployeesComponent implements OnInit {

    private subscription: Subscription = new Subscription();
    private userRole: string = this.userService.getRole();
    employees: Employee[];
    facilities: Facility[];
    employeeRoles: { key: string, value: string }[] = this.employeeService.getEmploeesRolesList();
    selectedFacilityId: number;

    sortKey: string = ''

    constructor(private route: ActivatedRoute,
        private router: Router,
        private breadcrumbsService: BreadcrumbsService,
        private dialog: MatDialog,
        private toolboxService: ToolboxService,
        private employeeService: EmployeeService,
        private facilityService: FacilityService,
        private userService: UserService,
        private global: Global) { }

    ngOnInit() {
        this.breadcrumbsService.setStep(1, 'Employees', this.router.url);
        this.getFacilities();
    }

    getFacilities(): void {
        this.subscription.add(
            this.facilityService.getFacilityList().subscribe(facilities => {
                this.facilities = facilities;
                if (facilities.length > 0) {
                    let firstfacilityId = facilities[0].id;
                    this.selectedFacilityId = firstfacilityId;
                    this.facilityChanged(firstfacilityId)
                }
                console.log(this.facilities);
            })
        );
    }

    facilityChanged(facilityId: number): void {
        console.log("facility changed: " + facilityId);
        this.getEmployess(facilityId);
    }

    getEmployess(facilityId: number): void {
        this.subscription.add(
            this.employeeService.getEmployeeListForFacilityId(facilityId).subscribe(employees => {
                console.log("Employees: ", employees);
                this.employees = this.filterToRole(employees);
            })
        );
    }

    addNewEmployee(): void {
        const dialogRef = this.dialog.open(AddEmployeeDialogComponent, {
            width: '550px',
        });
        this.subscription.add(
            dialogRef.afterClosed().subscribe(employee => {
                if (!employee) return;
                console.log(employee);
                this.subscription.add(
                    this.employeeService.addEmployee(employee).subscribe(() => {
                        this.toolboxService.showSuccess('New employee has been invited ',
                            'The new staff member who you just created will receive an invitation email to the indicated address. You can look at the status of the account via the employees tab.');

                        this.getEmployess(this.selectedFacilityId);
                      },
                      err => {
                        var msg = err.error.message == 'invite-exists' ? "Can't create employee with email which was already used. " : "";
                        msg += err.error.error + " " + err.error.status;
                        this.toolboxService.showError("Error", msg);
                        throw err;
                      }
                    )
                );
            })
        );
    }

    sortBy(orderKey: string): void {
        this.employees = _.orderBy(this.employees, orderKey.split(','), this.sortKey === orderKey ? 'asc' : 'desc');
        this.sortKey = this.sortKey === orderKey ? '' : orderKey;
    }

    getRoleByKey(key: string): string {
        let el = this.employeeRoles.find(el => el.key === key.toUpperCase());
        return el ? el.value : '';
    }

    filterToRole(employees: Employee[]): Employee[]{
        switch(this.userRole.toUpperCase()){
            case 'COMPANY_HEAD':
            case 'TECHNICAL_SUPPORT':
            return employees;
            case 'FACILITY_HEAD':
            return employees.filter(el => el.user_role.toUpperCase() !== 'FACILITY_HEAD');
            case 'SECTION_HEAD':
            return employees.filter(el => el.user_role.toUpperCase() !== 'FACILITY_HEAD' && el.user_role.toUpperCase() !== 'SECTION_HEAD');
            case 'CAREGIVER':
            return employees.filter(el => el.user_role.toUpperCase() !== 'FACILITY_HEAD' && el.user_role.toUpperCase() !== 'SECTION_HEAD' && el.user_role.toUpperCase() !== 'CAREGIVER');

        }
        return employees;
    }

    reinviteEmployee(employeeEmail: string): void {
       console.log(employeeEmail);
        this.subscription.add(
            this.employeeService.reinviteEmployee(employeeEmail).subscribe(() => {
                console.log('reinviteEmployee');
            })
        );
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
