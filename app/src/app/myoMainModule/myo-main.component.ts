import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';

import { ToolboxService } from '../_common/toolbox.service';
import { BreadcrumbsService } from '../breadcrumbsComponent/services/breadcrumbs.service';
import { UserService } from './_services/user/user.service';
import { User } from './_classes/user.class';
@Component({
	selector: 'app-myo-main',
	templateUrl: './myo-main.component.html',
	styleUrls: ['./myo-main.component.css']
})

export class MyoMainComponent implements OnInit {

	user: User;
	private subscription: Subscription = new Subscription();

	constructor(private router: Router,
		private toolboxService: ToolboxService,
		private breadcrumbsService: BreadcrumbsService,
		private userService: UserService) { }

	ngOnInit() {
		this.getUser();
		this.breadcrumbsService.setStep(0, 'Company', '/facility');

	}

	getUser(): void {
		this.subscription.add(
			this.userService.getUser().subscribe(user => {
				this.userService.user = user;
				this.user = user;
				this.setRootLink();
				console.log(this.user)
			})
		);
	}

	setRootLink(): void {
		let root: string;
		console.log(this.userService.getRole().toUpperCase());
		switch(this.userService.getRole().toUpperCase()){
			case 'COMPANY_HEAD':
			case 'TECHNICAL_SUPPORT':
			root = '/facility';
			break;
            case 'FACILITY_HEAD':
			root = `/facility/${this.user.facility_id}`;
			// root = `/facility/5`;
			break;
            case 'SECTION_HEAD':
			root = `/facility/${this.user.facility_id}/section/${this.user.section_id}`;
			// root = `/facility/5/section/6`;
			break;
			case 'CAREGIVER':
			root = '/caregiver';
			this.accessAdminPanel();
			break;
		}

		console.log(root);
		this.userService.setRootLink(root);
		this.navigateToRoot();
	}

	navigateToRoot(): void{
		this.userService.getRootLink().subscribe(link =>{
			const url: string = this.toolboxService.appUrl === '/' ? link : this.toolboxService.appUrl;
			// console.log(url);
			this.router.navigate([url]);
		});

	}

	accessAdminPanel(): void {
		const header: string = 'Info';
		const text: string = 'Caregivers have no access Admin Panel.';
		this.toolboxService.showSuccess(header, text);
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}


}


