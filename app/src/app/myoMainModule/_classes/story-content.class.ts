import { Media } from "./content-media.class";

export class StoryContent {
  media: Media[];
  description: string;
  type: number;
  typeName: string;
  //media: List<StoryListResponseItemMediaDto>
}
