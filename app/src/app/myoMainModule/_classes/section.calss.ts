export class Section {
	id: number;
	name: string;
	seniorsNum: number;
	relatives: number;
	caretakersNum: number;
	postsLastWeekNum: number;

	constructor(name: string) {
		this.name = name;
		this.seniorsNum = 0;
		this.relatives = 0;
		this.caretakersNum = 0;
		this.postsLastWeekNum = 0;
	}
}