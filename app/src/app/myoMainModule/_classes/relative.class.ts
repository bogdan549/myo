
export class Relative {
	id: number;
	first_name: string;
	last_name: string;
	relation_type_id: number;
	email: string;
	phone_number:string;
	invite_pending: boolean;
	image: {
        preview: string,
        image: string
    };
	constructor() {
		
	}
}