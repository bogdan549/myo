export class Media {
    content_type: string;
    location: string;
    preview: string;
    unauthenticated: string;
};
