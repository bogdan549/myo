import { Relative } from './relative.class';

export class EmployeeProfile {
	id: number;
	first_name: string;
	last_name: string;
	avatar: string;
	email: string;
	phone: string;
	place: string;
	birthday: string;
	prehobby: string;
	tfacil: string;
	hobbi: string;
	motiv: string;
	stories: object[];
	status: object[];
	facilities: object[];
	constructor() {

	}
};
