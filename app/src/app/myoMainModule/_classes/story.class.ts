import { StoryContent } from "./story-content.class";
import { Caretaker } from "./caretaker.class";
import { SectionSenior } from "./senior.class";

export class Story {
	id: number;
	content: StoryContent;
	author: string;
	category: number; //categpory Id here
	type: string;
	isInternal: boolean;
	isUploadComplete: boolean;
	recipients: SectionSenior[];
	accepted: boolean;
	posted_on: number;
	posted_by: Caretaker;
	status: string;

	constructor() {

	}
}
