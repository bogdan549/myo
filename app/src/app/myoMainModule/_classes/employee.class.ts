export class Employee {
    id: number;
    image: string;
    email: string;
    date_of_birth:string;
    employee_count:number;
    facility_id:number;
    facility_name:string;
    facility_start_year:number;
    first_name: string;
    last_name: string;
    hobbies:string;
    is_legal_guardian:boolean;
    invite_status: string;
    motivation:string;
    phone_number: string;
    place_of_birth: {
        city:string,
        country:string
    }
    previous_hobbies: string;
    relation_id:number;
    relative_count:number;
    section_id:number;
    section_name: string;
    senior: any;
    senior_count:number;
    user_role: EmployeeRole;
    approval_required: boolean;


    constructor(id: number, firstName: string, lastName: string, role: string, section_name: string) {
        console.log(role);
        this.id = id;
        this.image = 'http://via.placeholder.com/100x100/dddddd';
        this.first_name = firstName;
        this.last_name = lastName;
        //this.status = 'invited';
        this.user_role = EmployeeRole[role];
        this.section_name = section_name;
        //this.sections = [];
    }
}

export enum EmployeeRole {
    COMPANY_HEAD  = 'COMPANY_HEAD',
    FACILITY_HEAD = 'FACILITY_HEAD',
    SECTION_HEAD = 'SECTION_HEAD',
    CAREGIVER = 'CAREGIVER',
    // EMPLOYEE = 'employee'

}
