export class Facility {
	id: number;
	image: string;
	name: string;
	sectionsNum: number;
	seniorsNum: number;
	caretakersNum: number;
	companyId: number;
	companyName: string;

	constructor(name: string) {
		this.image = 'http://via.placeholder.com/100x100/dddddd';
		this.name = name;
		this.sectionsNum = 0;
		this.seniorsNum = 0; 
		this.caretakersNum = 0;
	}
}