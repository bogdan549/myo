export class SeniorProfile {
	date_of_birth: string;
	facility_name: string;
	favorite_books: string;
	favorite_colors: string;
	favorite_movies: string;
	favorite_music: string;
	first_name: string;
	hobbies: string;
	id: number;
	image: {
		image: string,
		preview: string,
	};
	last_job:string;
	last_name: string;
	legal_guardian: {
		first_name:string,
		last_name: string,
		phone_number:string
	};
	place_of_birth: {
		city:string,
		country:string
	};
	section_id: number;
	section_name: string;

	constructor() {
	}
}
