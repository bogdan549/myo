
export class Admin {

	date_of_birth:string;
	email:string;
	employee_count:number;
	facility_id:number;
	facility_name:string;
	facility_start_year:number;
	first_name:string;
	hobbies:string;
	id:number;
	image:string;
	invite_status:string;
	is_legal_guardian:boolean;
	last_name: string;
	motivation:string;
	phone_number:string;
	place_of_birth:{
		city: string, 
		country: string
	}
	previous_hobbies:string;
	relation_id:number;
	relative_count:number;
	section_id:number;
	section_name:string;
	senior:string;
	senior_count:number;
	user_role:AdminRole

	constructor() { }

};

export enum AdminRole {
    COMPANY_HEAD  = 'Company Head',
    FACILITY_HEAD = 'Facility Head',
    SECTION_HEAD = 'Section Head',
    TECHNICAL_SUPPORT = 'Technical Support',
}