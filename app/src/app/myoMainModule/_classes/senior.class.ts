export class SectionSenior {
	id: number;
	first_name: string;
	last_name: string;
	image: {
		preview: string,
		image: string;
	};
	posts: number;
	relatives: number;
	facility_name: string;
	facility_id: number;
	section_name: string;
	section_id: number;

	constructor(firstName: string, lastName: string) {
		this.id = 0,
		this.first_name = firstName,
		this.last_name = lastName;
		this.image = {
			preview: '',
			image: '',
		};
		this.facility_name = '',
		this.facility_id = 0,
		this.section_name = '',
		this.section_id = 0,
		this.posts = 0,
		this.relatives = 0

	}
}
