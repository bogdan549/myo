
export class User {
    company_id: number;
    company_name:string;
	date_of_birth:string;
    email:string;
    employee_count:number;
    facility_id: number;
    facility_start_year:string;
    first_name:string;
    hobbies:string;
    image:string;
    is_legal_guardian:boolean;
    last_name:string;
    motivation:string;
    phone_number:string;
    place_of_birth:string;
    relation_id:number;
    relative_count:number;
    section_id: number;
    section_name:string;
    senior:string;
    senior_count:number;
    user_role:string;

	constructor() { }

};