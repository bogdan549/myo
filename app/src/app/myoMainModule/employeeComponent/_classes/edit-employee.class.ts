
export class EditEmployee {
    first_name: string;
    last_name: string;
    place_of_birth: string;
    phone_number: string;
    country_of_birth: string;
    date_of_birth: string;
    hobbies: string;
    facility_start_year: number;
    previous_hobbies: string;
    motivation: string;
    relation_id: number;
    postId: string;
    type: string;
    approval_required: boolean;

    constructor() {
        this.first_name = '',
        this.last_name = '',
        this.place_of_birth = '',
        this.country_of_birth = '',
        this.phone_number = '',
        this.date_of_birth = '2005-07-31',
        this.hobbies = '',
        this.facility_start_year = 1900,
        this.previous_hobbies = '',
        this.motivation = '',
        this.relation_id = null,
        this.postId = '',
        this.type = ''
    }
}
