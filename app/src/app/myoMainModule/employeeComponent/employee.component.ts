import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { BreadcrumbsService } from '../../breadcrumbsComponent/services/breadcrumbs.service';
import { Employee, EmployeeRole } from "../_classes/employee.class";
import { EmployeeService } from "../_services/employee/employee.service";
import { Facility } from "../_classes/facility.class";
import { FacilityService } from "../_services/facility/facility.service";
import { Section } from '../_classes/section.calss';
import { Company } from '../_classes/company.class';
import { SectionService } from '../_services/section/section.service';
import { CompanyService } from '../_services/company/company.service';
import { ToolboxService } from '../../_common/toolbox.service';
import { Global } from '../_services/global/global';
import { UserService } from '../_services/user/user.service';
import { EditEmployee } from './_classes/edit-employee.class';

@Component({
    selector: 'app-employee',
    templateUrl: './employee.component.html',
    styleUrls: ['./employee.component.css'],
    providers: [ Global ]
})
export class EmployeeComponent implements OnInit {

    private subscription: Subscription = new Subscription();
    employeeId: number;
    userRole: string;
    employee: Employee;
    private defaultEmployee: EditEmployee;
    editBlock: string = '';
    facilities: Facility[] = [];
    sections: Section[] = [];
    companies: Company[] = [];
    employeeRoles: { key: string, value: string }[] = this.employeeService.getEmploeesRolesList();
    approvalSettings: { value: boolean, description: string }[] = [
      {
        value: true,
        description: "Approval required"
      },
      {
        value: false,
        description: "Approval not required"
      }
    ];

    selectedFacilityId: number;
    selectedSectionId: number;
    facilitySelectDisabled: boolean = true;
    sectionSelectDisabled: boolean = true;
    leave: boolean = false;
    profileSaving: boolean = false;

    editedEmployee: FormGroup = new FormGroup({
        first_name: new FormControl(),
        last_name: new FormControl(),
        phone_number: new FormControl(),
        country_of_birth: new FormControl(),
        place_of_birth: new FormControl(),
        date_of_birth: new FormControl(),
        facility_start_year: new FormControl(),
        hobbies: new FormControl(),
        previous_hobbies: new FormControl(),
        motivation: new FormControl(),
        relation_id: new FormControl(),
        postId: new FormControl(),
        type: new FormControl(),
        approvalRequired: new FormControl(),
    });

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private breadcrumbsService: BreadcrumbsService,
        private employeeService: EmployeeService,
        private fb: FormBuilder,
        private facilityService: FacilityService,
        private sectionService: SectionService,
        private companyService: CompanyService,
        private toolboxService: ToolboxService,
        private location: Location,
        private global: Global,
        private userService: UserService
      ) { }

    ngOnInit() {
        this.employeeId = +this.route.snapshot.paramMap.get('employeeId');
        this.userRole = this.route.snapshot.paramMap.get('userRole');
        this.getEmployee();
        this.getFacilities();
        this.getCompanies();
    }

    getEmployee(): void {
        this.subscription.add(
            this.employeeService.getEmployee(this.employeeId, this.userRole).subscribe(employee => {
                this.employee = employee;
                //this.defaultEmployee = JSON.parse(JSON.stringify(this.employee));
                console.log('emp', this.employee);
                this.setDefaultEmployeeObject();
                this.breadcrumbsService.setStep(1, 'Employees', '/employees');
                this.breadcrumbsService.setStep(2, employee.first_name + ' ' + employee.last_name, this.router.url);
                this.setEditEmployeeForm();
                this.getSections(employee.facility_id);
                this.changeEmployeeRole(EmployeeRole[this.employee.user_role.toUpperCase()]);
            })
        );
    }
    setDefaultEmployeeObject(): void {
      this.defaultEmployee = {
          first_name: this.employee.first_name,
          last_name: this.employee.last_name,
          phone_number: this.employee.phone_number,
          place_of_birth: this.employee.place_of_birth.city,
          country_of_birth: this.employee.place_of_birth.country,
          date_of_birth: this.employee.date_of_birth,
          hobbies: this.employee.hobbies,
          facility_start_year: this.employee.facility_start_year,
          previous_hobbies: this.employee.previous_hobbies,
          motivation: this.employee.motivation,
          relation_id: this.employee.relation_id,
          postId: '',
          type: this.employee.user_role.toUpperCase(),
          approval_required: this.employee.approval_required,
      };
  }


    setEditEmployeeForm(): void {
        this.editedEmployee = this.fb.group({
            first_name: [this.employee.first_name, Validators.required],
            last_name: [this.employee.last_name, Validators.required],
            phone_number: [this.employee.phone_number],
            country_of_birth: [this.employee.place_of_birth.country],
            place_of_birth: [this.employee.place_of_birth.city],
            date_of_birth: [this.employee.date_of_birth],
            facility_start_year: [this.employee.facility_start_year],
            hobbies: [this.employee.hobbies],
            previous_hobbies: [this.employee.previous_hobbies],
            motivation: [this.employee.motivation],
            relation_id: [null],
            postId: [''],
            type: [this.employee.user_role.toUpperCase()],
            approvalRequired: [this.employee.approval_required ? true : false],
        });
        this.selectedFacilityId = this.employee.facility_id;
        this.selectedSectionId = this.employee.section_id;
    }

    editField(block: string): void {
        this.editBlock = block;
    }

    confirmEdit(): void {
        this.editBlock = '';
    }

    getFacilities(): void {
        this.subscription.add(
            this.facilityService.getFacilityList().subscribe(facilities => {
                this.facilities = facilities;
                if(facilities.length == 1){
                  this.selectedFacilityId = facilities[0].id;
                }
            })
        );
    }

    getSections(facilityId: number): void {
        this.subscription.add(
            this.sectionService.getFacilitySections(facilityId).subscribe(sections => {
                this.sections = sections;
                if(sections.length == 1){
                  this.selectedSectionId = sections[0].id;
                }
            })
        );
    }

    getCompanies(): void {
        this.subscription.add(
            this.companyService.getCompanyList().subscribe(companies => {
                this.companies = companies;
            })
        );
    }

    saveEmployeeDetails(): void {
        this.setPostId();
        console.log('editEmp', this.editedEmployee.value);
        if (this.editedEmployee.invalid) {
            return;
        }
        const header: string = 'Success';
        const text: string = 'Empoyee profile changed successfully';
        this.profileSaving = true;
        this.employeeService.editEmployee(this.editedEmployee.value, this.employee.id).subscribe((response) => {
              this.profileSaving = false;
              this.toolboxService.showSuccess(header, text);
            },
          (error) => {
              this.profileSaving = false;
              this.toolboxService.showError('Error', 'Profile could not be saved at this time');
          });
    }

    isProfileChanged(): boolean {
      for (let key of Object.keys(this.defaultEmployee)) {
         if (this.editedEmployee.value[key] !== this.defaultEmployee[key]) {
             console.log(key);
             return true;
         }
     }
     if( this.employee.facility_id !== null &&  this.employee.section_id !== null)
      if(this.selectedFacilityId !== this.employee.facility_id || this.selectedSectionId !== this.employee.section_id )
          return true;
     console.log('NOTchanged');
     return false;
   }


    cancelEmployeeEdit(): void {
        // this.employee = JSON.parse(JSON.stringify(this.defaultEmployee));
        //
        // this.setEditEmployeeForm();

        this.isProfileChanged() == true ? this.confirmProfileChanges() : this.location.back();
        // this.employee = JSON.parse(JSON.stringify(this.defaultEmployee));
        // this.setEditEmployeeForm();
    }

    confirmProfileChanges(): void{
      const header: string = 'Information not saved yet';
      const text: string = 'There are changes, are you sure you want to cancel?';

      this.toolboxService.confirm(header, text).subscribe(res => {
          if (res) {
              this.leave = true;
              this.location.back();
          }
      });
    }
    // onAddingSection(tag: { id: number, name: string }): void {
    //     this.choosenSections.push({ id: tag.id, name: tag.name });
    //     console.log(tag);
    // }

    // onRemovingSection(tag: { id: number, name: string }): void {
    //     let index_of_section = this.choosenSections.findIndex(x => x.id == tag.id);
    //     this.choosenSections.splice(index_of_section, 1);
    //     console.log(tag);
    // }

    setPostId(): void {
        switch (this.editedEmployee.value.type.toUpperCase()) {
            case 'TECHNICAL_SUPPORT':
            case 'COMPANY_HEAD':
                this.editedEmployee.controls.postId.setValue(this.companies[0].id);
                break;
            case 'FACILITY_HEAD':
                this.editedEmployee.controls.postId.setValue(this.selectedFacilityId);
                break;
            case 'SECTION_HEAD':
                this.editedEmployee.controls.postId.setValue(this.selectedSectionId);
                break;
            case 'CAREGIVER':
                this.editedEmployee.controls.postId.setValue(this.selectedSectionId);
                break;
        }
    }

    changeEmployeeRole(role: string): void {
        switch (role) {
            case 'COMPANY_HEAD':
            case 'TECHNICAL_SUPPORT':
                this.facilitySelectDisabled = true;
                this.sectionSelectDisabled = true;
                this.selectedFacilityId = -1;
                this.selectedSectionId = -1;
                break;
            case 'FACILITY_HEAD':
                this.facilitySelectDisabled = false;
                this.sectionSelectDisabled = true;
                this.selectedFacilityId = this.employee.facility_id;
                this.selectedSectionId = -1;
                break;
            case 'SECTION_HEAD':
                this.facilitySelectDisabled = false;
                this.sectionSelectDisabled = false;
                this.selectedFacilityId = this.employee.facility_id;
                this.selectedSectionId = this.employee.section_id;
                break;
            case 'CAREGIVER':
                this.facilitySelectDisabled = false;
                this.sectionSelectDisabled = false;
                this.selectedFacilityId = this.employee.facility_id;
                this.selectedSectionId = this.employee.section_id;
                break;
        }
    }

    deleteEmployee(): void {
        const header: string = 'Delete employee’s profile';
        const text: string = 'This employee’s profile will be deleted. Would you like to proceed?';
        this.subscription.add(
            this.toolboxService.confirm(header, text).subscribe(res => {
                if (res) {
                    this.subscription.add(
                        this.employeeService.deleteEmployee(this.employee.id).subscribe(response => {
                            this.location.back();
                        })
                    );
                }
            })
        );
    }

    reinviteEmployee(employeeEmail: string): void {
       console.log(employeeEmail);
        this.subscription.add(
            this.employeeService.reinviteEmployee(employeeEmail).subscribe(() => {
                console.log('reinviteEmployee');
            })
        );
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
