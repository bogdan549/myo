export class EditSenior {
    first_name: string;
    last_name: string;
    place_of_birth: string;
    country_of_birth: string;
    date_of_birth: string;
    hobbies: string;
    last_job: string;
    favourite_movies: string;
    favourite_music: string;
    favourite_books: string;
    favourite_colors: string;
    section_id_to_set: number;

    constructor(firstName: string, lastName: string, sectionId: number) {
        this.first_name = firstName,
        this.last_name = lastName,
        this.place_of_birth = '',
        this.country_of_birth = '',
        this.date_of_birth = '2005-07-31',
        this.hobbies = '',
        this.last_job = '',
        this.favourite_movies = '',
        this.favourite_music = '',
        this.favourite_books = '',
        this.favourite_colors = '',
        this.section_id_to_set = sectionId
    }
}