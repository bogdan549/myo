import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription, Observable, Subject } from 'rxjs';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

import { SeniorProfile } from '../_classes/senior-profile.class';
import { Section } from '../_classes/section.calss';
import { EditSenior } from './_classes/edit-senior.class';
import { BreadcrumbsService } from '../../breadcrumbsComponent/services/breadcrumbs.service';
import { SeniorService } from '../_services/senior/senior.service';
import { ToolboxService } from '../../_common/toolbox.service';
import { SectionService } from '../_services/section/section.service';
import { FacilityService } from "../_services/facility/facility.service";
import { Facility } from "../_classes/facility.class";
import {CountryService} from "../_services/country/country.service";
import { Global } from '../_services/global/global';

@Component({
    selector: 'app-senior',
    templateUrl: './senior.component.html',
    styleUrls: ['./senior.component.css'],
    providers: [ Global ]
})
export class SeniorComponent implements OnInit {

    private subscription: Subscription = new Subscription();
    seniorProfile: SeniorProfile;
    seniorId: number;
    facilityId: number;
    seniorAge: number;
    editBlock: string;
    sectionList: Section[];
    facilities: Facility[];
    countries: string[];
    selectedFacilityId: number;
    profileSaving: boolean = false;
    selectedSectionId: number;
    leave: boolean = false;

    editSeniorForm: FormGroup = new FormGroup({
        first_name: new FormControl(),
        last_name: new FormControl(),
        place_of_birth: new FormControl(),
        country_of_birth: new FormControl(),
        date_of_birth: new FormControl(),
        hobbies: new FormControl(),
        last_job: new FormControl(),
        favourite_movies: new FormControl(),
        favourite_music: new FormControl(),
        favourite_books: new FormControl(),
        favourite_colors: new FormControl(),
        section_id_to_set: new FormControl()
    });
    private redirectSubject: Subject<boolean> = new Subject();

    private defaultValue: EditSenior;

    constructor(private router: Router,
        private route: ActivatedRoute,
        private breadcrumbsService: BreadcrumbsService,
        private facilityService: FacilityService,
        private seniorService: SeniorService,
        private toolboxService: ToolboxService,
        private location: Location,
        private fb: FormBuilder,
        private countryService: CountryService,
        private sectionService: SectionService,
        private global: Global) {
    }

    ngOnInit() {
        this.seniorId = +this.route.snapshot.paramMap.get('seniorId');
        this.facilityId = +this.route.snapshot.paramMap.get('facilityId');
        this.getSenior();
        this.getSectionList(this.facilityId);
        this.getFacilities();
        this.getCountries();
        this.selectedFacilityId = this.facilityId;

    }

    getSenior(): void {
        this.subscription.add(
            this.seniorService.getSenior(this.seniorId).subscribe(profile => {
                console.log('Senior profile: ', profile);
                this.seniorProfile = profile;
                this.breadcrumbsService.setStep(3, this.seniorProfile.first_name + ' ' + this.seniorProfile.last_name, this.router.url);
                this.countSeniorAge(this.seniorProfile.date_of_birth);
                this.fillForm();
                this.setDefaultSeniorObject();
            })
        );
    }

    setDefaultSeniorObject(): void {
        this.defaultValue = {
            first_name: this.seniorProfile.first_name,
            last_name: this.seniorProfile.last_name,
            place_of_birth: this.seniorProfile.place_of_birth.city,
            country_of_birth: this.seniorProfile.place_of_birth.country,
            date_of_birth: this.seniorProfile.date_of_birth,
            hobbies: this.seniorProfile.hobbies,
            last_job: this.seniorProfile.last_job,
            favourite_movies: this.seniorProfile.favorite_movies,
            favourite_music: this.seniorProfile.favorite_music,
            favourite_books: this.seniorProfile.favorite_books,
            favourite_colors: this.seniorProfile.favorite_colors,
            section_id_to_set: this.seniorProfile.section_id,
        };

    }

    getFacilities(): void {
      this.facilityService.getFacilityList().subscribe( facilities => {
        this.facilities = facilities;
      });
    }

    getCountries(): void {
      this.subscription.add(
        this.countryService.getCountries().subscribe( countries => {
          this.countries = countries;
        })
      );
    }

    getSectionList(facilityId: number, facilityChanged: boolean = false): void {
        this.subscription.add(
            this.sectionService.getFacilitySections(facilityId).subscribe(list => {
                this.sectionList = list;
                if(facilityChanged){
                    this.editSeniorForm.controls.section_id_to_set.setValue('');
                }
                //let's allow user to save without touching dropdown
                if(this.sectionList.length > 0){
                    this.editSeniorForm.controls.section_id_to_set.setValue(this.sectionList[0].id);
                }
                console.log('sections',list);
            })
        );
    }

    fillForm(): void {
        this.editSeniorForm = this.fb.group({
            first_name: [this.seniorProfile.first_name, Validators.required],
            last_name: [this.seniorProfile.last_name, Validators.required],
            place_of_birth: [this.seniorProfile.place_of_birth.city],
            country_of_birth: [this.seniorProfile.place_of_birth.country],
            date_of_birth: [this.seniorProfile.date_of_birth],
            hobbies: [this.seniorProfile.hobbies],
            last_job: [this.seniorProfile.last_job],
            favourite_movies: [this.seniorProfile.favorite_movies],
            favourite_music: [this.seniorProfile.favorite_music],
            favourite_books: [this.seniorProfile.favorite_books],
            favourite_colors: [this.seniorProfile.favorite_colors],
            section_id_to_set: [this.seniorProfile.section_id],
        });
    }


    countSeniorAge(date: string): void {
        this.seniorAge = Math.floor((new Date(Date.now()).getTime() - (new Date(date)).getTime()) / (1000 * 60 * 60 * 24 * 365));
    }

    deleteSenior(): void {
        const header: string = 'Delete senior’s profile';
        const text: string = 'When deleting this senior’s account, all information and content linked to the senior is deleted. Would you like to proceed?';
        this.subscription.add(
            this.toolboxService.confirm(header, text).subscribe(res => {
                console.log(res);
                if (res) {
                    this.subscription.add(
                        this.seniorService.deleteSenior(this.seniorId).subscribe(response => {
                            console.log(response);
                            this.location.back();
                        })
                    );
                }
            })
        );
    }

    editField(block: string): void {
        this.editBlock = block;
    }

    confirmEdit(): void {
        this.editBlock = '';
    }

    editSenior(): void {
        if(this.editSeniorForm.invalid) return;

      this.profileSaving = true;
        this.subscription.add(
            this.seniorService.updateSenior(this.editSeniorForm.value, this.seniorProfile.id).subscribe(
                response => {
                    this.defaultValue = this.editSeniorForm.value;
                    this.profileSaving = false;
                    this.toolboxService.showSuccess('Success', 'Profile Saved');
                },
                err => {
                    this.profileSaving = false;
                    this.toolboxService.showError('Error', 'Profile could not be saved at this time');
                }
            )
        );
    }

    isProfileChanged(): boolean {
        for (let key of Object.keys(this.defaultValue)) {
            if (this.editSeniorForm.value[key] !== this.defaultValue[key]) {
                console.log(key);
                return true;
            }
        }
        console.log('NOTchanged');
        return false
    }

    confirmProfileChanges(): void {
        const header: string = 'Information not saved yet';
        const text: string = 'There are changes, are you sure you want to cancel?';
        this.toolboxService.confirm(header, text).subscribe(res => {
            if (res) {
                this.leave = true;
                this.location.back();
            }
        })
    }

    cancel(): void {
        if (this.isProfileChanged()) {
            this.confirmProfileChanges();
        } else {
            this.location.back();
        }
    }

    canDeactivate(): Observable<boolean> | boolean {
        if (!this.isProfileChanged() || this.leave) {
            return true;
        }

        this.confirmProfileChanges();
        return this.redirectSubject.asObservable();

    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
