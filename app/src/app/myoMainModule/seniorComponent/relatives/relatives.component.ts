import { Component, OnInit, Input } from '@angular/core';
import { Subscription } from 'rxjs';

import { SeniorService } from '../../_services/senior/senior.service';
import { Global } from '../../_services/global/global';
import { ToolboxService } from '../../../_common/toolbox.service';
import { Relative } from '../../_classes/relative.class';

@Component({
    selector: 'app-relatives',
    templateUrl: './relatives.component.html',
    styleUrls: ['./relatives.component.css'],
    providers: [ Global ]
})
export class RelativesComponent implements OnInit {

    @Input() seniorId: number;

    private subscription: Subscription = new Subscription();
    relatives: Relative[];


    constructor(private seniorService: SeniorService,
        private toolboxService: ToolboxService,
        private global: Global) { }

    ngOnInit() {
        this.getSeniorRelatives();

    }

    getSeniorRelatives(): void {
        this.subscription.add(
            this.seniorService.getSeniorRelatives(this.seniorId).subscribe(relatives => {
                console.log('reatives: ', relatives);
                this.relatives = relatives;
            })
        );
    }

    deleteRelative(relativeId: number): void {
        const header: string = 'Delete relative profile';
        const text: string = 'When deleting this relative’s account, all information and content linked to this profile is deleted. The relative won’t have further access. Would you like to proceed?';
        this.subscription.add(
            this.toolboxService.confirm(header, text).subscribe(confirmed => {
                if(confirmed) {
                    this.subscription.add(
                        this.seniorService.removeRelative(this.seniorId, relativeId).subscribe(() => {
                            this.relatives = this.relatives.filter(el => el.id !== relativeId);
                        })
                    );
                }
            })
        );
    }

    reinviteRelative(relativeEmail: string): void {
       console.log(relativeEmail);
        this.subscription.add(
            this.seniorService.reinviteRelative(relativeEmail).subscribe(() => {
                console.log('reinviteRelative');
            })
        );
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
