import { Component, OnInit,Inject } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { DatePipe } from '@angular/common';

import { Facility } from '../../_classes/facility.class';
import { Section } from '../../_classes/section.calss';
import { NewSenior } from '../_classes/new-senior.class';
import { Relation } from '../../_classes/relations.class';

import { FacilityService } from '../../_services/facility/facility.service';
import { SectionService } from '../../_services/section/section.service';
import { StaticService } from '../../_services/static/static.service';
import { ToolboxService } from '../../../_common/toolbox.service';

@Component({
    selector: 'app-add-senior-dialog',
    templateUrl: './add-senior-dialog.component.html',
    styleUrls: ['./add-senior-dialog.component.css']
})
export class AddSeniorDialogComponent implements OnInit {

    private subscription: Subscription = new Subscription();
    facilityList: Facility[];
    sectionList: Section[];
    relationshipList: Relation[];
    private defaulSeniorObj: any;

    newSenior: FormGroup = new FormGroup({
        first_name: new FormControl(),
        last_name: new FormControl(),
        facility: new FormControl(),
        section: new FormControl(),
        guardianFirstName: new FormControl(),
        guardianLastName: new FormControl(),
        guardianEmail: new FormControl(),
        // guardianPassword: new FormControl(),
        // guardianPhoneNumber: new FormControl(),
        // guardianRelationship: new FormControl(),
    });
    constructor(private fb: FormBuilder,
        private facilityService: FacilityService,
        private sectionService: SectionService,
        @Inject(MAT_DIALOG_DATA) public data: {facilityId: number, facilityName: string},
        private dialogRef: MatDialogRef<AddSeniorDialogComponent>,
        private staticService: StaticService,
        private toolboxService: ToolboxService,
        private datePipe: DatePipe) {
            dialogRef.disableClose = true;
        }

    ngOnInit() {
        this.getFacilityList();
        this.getFacilitySections(this.data.facilityId);
        this.getRelationList();
        this.newSenior = this.fb.group({
            first_name: ['', Validators.required],
            last_name: ['', Validators.required],
            facility: [this.data.facilityId, Validators.required],
            section: ['', Validators.required],
            guardianFirstName: ['', Validators.required],
            guardianLastName: ['', Validators.required],
            guardianEmail: ['', Validators.required],
            // guardianPassword: ['', Validators.required],
            // guardianPhoneNumber: ['', Validators.required],
            // guardianRelationship: ['', Validators.required]
        });
        // this.newSenior.controls.facility.disable({ onlySelf: true });
        console.log(this.data);
        this.defaulSeniorObj = this.newSenior.value;

        this.subscription.add(
            this.dialogRef.backdropClick().subscribe(() => {
                this.tryToCloseDialog();
            })
        );
    }

    tryToCloseDialog(): void {
        if(this.checkForSeniorChange()){
            const header: string = 'Information not saved yet';
            const text: string = 'You haven’t saved the changed you have made. If you proceed now your changes won’t be saved.';
            this.subscription.add(
                this.toolboxService.confirm(header, text).subscribe(res => {
                    if (res) {
                        this.dialogRef.close();
                    }
                })
            );
        } else {
            this.dialogRef.close();
        }
    }

    checkForSeniorChange(): boolean {
        for (let key of Object.keys(this.defaulSeniorObj)) {
            if (this.newSenior.value[key] !== this.defaulSeniorObj[key]) {
                console.log(key);
                return true;
            }
        }
        console.log('NOTchanged');
        return false
    }

    getRelationList(): void {
        this.subscription.add(
            this.staticService.getRelationList().subscribe(list => {
                this.relationshipList = list;
            })
        );
    }

    getFacilityList(): void {
        this.subscription.add(
            this.facilityService.getFacilityList().subscribe(facilityList => {
                console.log(facilityList);
                this.facilityList = facilityList;
            })
        );
    }

    getFacilitySections(facilityId: number): void {
        this.subscription.add(
            this.sectionService.getFacilitySections(facilityId).subscribe(sectionList => {
                console.log(sectionList);
                this.sectionList = sectionList;
            })
        );
    }

    checkStep(keysArr: string[]): void{
        for(let i = 0; i < keysArr.length; i++){
            this.newSenior.controls[keysArr[i]].markAsTouched();
        }
    }

    finishStep(keysArr: string[]): void {
        this.checkStep(keysArr);
        if(this.newSenior.invalid) return;
        const newSenior: NewSenior = {
            facility_id: +this.newSenior.value.facility,
            facility_name: this.facilityList.find(el => el.id === +this.newSenior.value.facility).name,
            first_name: this.newSenior.value.first_name,
            id: null,
            image: {
                image: '',
                preview: ''
            },
            last_name: this.newSenior.value.last_name,
            section_id: +this.newSenior.value.section,
            section_name: this.sectionList.find(el => el.id = this.newSenior.value.section).name,
            guardian: {
                email: this.newSenior.value.guardianEmail,
                password: ''+(new Date()).getTime(),
                firstName: this.newSenior.value.guardianFirstName,
                lastName: this.newSenior.value.guardianLastName,
                phoneNumber: '',
                info: {
                    relationship: {
                        id: this.relationshipList.find(el => el.name.toLowerCase() === 'relative').id,
                        name: this.relationshipList.find(el => el.name.toLowerCase() === 'relative').name
                    },
                    isInnerCircle: true,
                    isLegalGuardian: true
                 }
            }
        }
        console.log(newSenior);
        this.dialogRef.close(newSenior);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }


}
