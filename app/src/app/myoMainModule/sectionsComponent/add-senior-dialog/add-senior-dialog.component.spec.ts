import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSeniorDialogComponent } from './add-senior-dialog.component';

describe('AddSeniorDialogComponent', () => {
  let component: AddSeniorDialogComponent;
  let fixture: ComponentFixture<AddSeniorDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSeniorDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSeniorDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
