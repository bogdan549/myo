import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { Subscription } from 'rxjs';
import _ from 'lodash';

import { AddSeniorDialogComponent } from './add-senior-dialog/add-senior-dialog.component';
import { ToolboxService } from '../../_common/toolbox.service';
import { BreadcrumbsService } from '../../breadcrumbsComponent/services/breadcrumbs.service';
import { SeniorService } from '../_services/senior/senior.service';
import { DataService } from '../_services/data/data.service';
import { Global } from '../_services/global/global';
import { SectionSenior } from '../_classes/senior.class';
import { NewSenior } from './_classes/new-senior.class';
import { Section } from '../_classes/section.calss';
import { Facility } from '../_classes/facility.class';

@Component({
	selector: 'app-section',
	templateUrl: './section.component.html',
	styleUrls: ['./section.component.css'],
	providers: [ Global ]
})
export class SectionComponent implements OnInit {

	private subscription: Subscription = new Subscription();
	seniorList: SectionSenior[] = [];
	facilityId: number;
	sectionId: number;
	facility: Facility;
	section: Section;

  sortKey: string = ''

	constructor(private route: ActivatedRoute,
		private router: Router,
		private dialog: MatDialog,
		private toolboxService: ToolboxService,
		private breadcrumbsService: BreadcrumbsService,
		private seniorService: SeniorService,
		private dataService: DataService,
		private global: Global) { }

	ngOnInit() {
		this.facilityId = +this.route.snapshot.paramMap.get('facilityId');
		this.sectionId = +this.route.snapshot.paramMap.get('sectionId');
		this.getSectionSeniorList();
		this.getFacility();
	}

	getFacility(): void {
        this.subscription.add(
            this.dataService.getFacility(this.facilityId).subscribe(facility => {
                this.facility = facility;
				this.breadcrumbsService.setStep(1, this.facility.name, `/facility/${this.facilityId}`);
				this.getSection();
            }, error => {
                console.log(error);
            })
        );
	}

	getSection(): void {
		this.subscription.add(
            this.dataService.getSection(this.sectionId).subscribe(section => {
                this.section = section;
                this.breadcrumbsService.setStep(2, this.section.name, this.router.url);
            }, error => {
                console.log(error);
            })
        );
	}

	getSectionSeniorList(): void {
		this.subscription.add(
			this.seniorService.getSectionSeniorList(this.sectionId).subscribe(list => {
				console.log('SectionList: ', list);
				this.seniorList = list;
			})
		);
	}

	addNewSenior(): void {
		const dialogRef = this.dialog.open(AddSeniorDialogComponent, {
			width: '450px',
			data: { facilityId: this.facilityId, facilityName: this.facility.name }
		});

		dialogRef.afterClosed().subscribe(newSenior => {
			if (!newSenior) return;
			delete (newSenior.id);
			delete (newSenior.image);
			this.subscription.add(
				this.seniorService.addSenior(newSenior).subscribe(response => {
					console.log(response);
					this.getSectionSeniorList();
					this.toolboxService.showSuccess('Senior successfully created! ',
						'The new elderly has been created and the first relative will shortly receive an inviation via their indicated e-mail address. ');
				})
			);
		});
	}

  sortBy(orderKey: string): void {
    console.log(this.sortKey === orderKey);
    this.seniorList = _.orderBy(this.seniorList, orderKey.split(','), this.sortKey === orderKey ? 'asc' : 'desc');
    this.sortKey = this.sortKey === orderKey ? '' : orderKey;
  }

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}

}


