import { Relation } from "../../_classes/relations.class";


export class NewSenior {
    facility_id: number;
    facility_name: string;
    first_name: string;
    id: number;
    image: {
        image: string,
        preview: string
    };
    last_name: string;
    section_id: number;
    section_name: string;

    guardian: {
	      email: string,
        password: string,
        firstName: string,
        lastName: string,
        phoneNumber: string,
        info: {
            relationship: Relation,
            isInnerCircle: boolean,
            isLegalGuardian: boolean
         }
    }

    constructor() {

    }

}
