import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { User } from '../../_classes/user.class';
import { AuthenticationService } from '../../../_common/authentication.service';
import { UserService } from '../../_services/user/user.service';
import { Global } from '../../_services/global/global';

@Component({
    selector: 'app-side-bar',
    templateUrl: './side-bar.component.html',
    styleUrls: ['./side-bar.component.css'],
    providers: [ Global ]
})
export class SideBarComponent implements OnInit {

    private subscription: Subscription = new Subscription()
    user: User;
    rootLink: string = '';

    constructor(private userService: UserService,
        private authenticationService: AuthenticationService,
        private global: Global) { }

    ngOnInit() {
        this.getUser();
        this.getRootLink();        
    }

    getUser(): void {
        this.user = this.userService.user;
        if(!this.user){
            this.subscription.add(
                this.userService.getUser().subscribe(user => {
                    this.user = user;
                })
            );
        }
    }

    getRootLink(): void {
        this.subscription.add(
            this.userService.getRootLink().subscribe(link => {
                this.rootLink = link;
                console.log(this.rootLink)
            })
        )
    }

    logout(): void {
        this.authenticationService.logout();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
