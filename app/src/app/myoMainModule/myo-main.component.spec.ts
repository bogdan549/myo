import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyoMainComponent } from './myo-main.component';

describe('MyoMainComponent', () => {
  let component: MyoMainComponent;
  let fixture: ComponentFixture<MyoMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyoMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyoMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
