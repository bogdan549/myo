import { NgModule } from '@angular/core';
import { ReactiveFormsModule }   from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { CreateCompanyComponent } from "./createCompanyComponent/createCompany.component";
import { LoginComponent } from './loginComponent/login.component';
import { RegistrationComponent } from './registrationComponent/registration.component';

@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule
    ],
    declarations: [
        CreateCompanyComponent,
        LoginComponent,
        RegistrationComponent
    ],
	exports: [
        CreateCompanyComponent,
        LoginComponent,
        RegistrationComponent
    ]
})
export class SharedModule { }