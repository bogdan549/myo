import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyoMainModule } from './myoMainModule/myo-main.module';

import { LoginComponent } from './loginComponent/login.component';
import { RegistrationComponent } from './registrationComponent/registration.component';
import { AppComponent } from './app.component';
import { MyoMainComponent } from './myoMainModule/myo-main.component';
import { CompanyComponent } from './myoMainModule/companyComponent/company.component';
import { SectionComponent } from './myoMainModule/sectionsComponent/section.component';
import { FacilityComponent } from './myoMainModule/facilityComponent/facility.component';
import { EmployeesComponent } from './myoMainModule/employeesComponent/employees.component';
import { SeniorComponent } from './myoMainModule/seniorComponent/senior.component';
import { PostsComponent } from './myoMainModule/postsComponent/posts.component';
import { EmployeeComponent } from './myoMainModule/employeeComponent/employee.component';
import { AdminsComponent } from './myoMainModule/adminsComponent/admins.component';
import { RestorePasswordComponent } from './restore-password/restore-password.component';
import { SeniorService } from './myoMainModule/_services/senior/senior.service';
import { UserService } from './myoMainModule/_services/user/user.service';
import {CreateCompanyComponent} from "./createCompanyComponent/createCompany.component";

const appRoutes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'register/:type/:email/:token/:date', component: RegistrationComponent },
    { path: 'register/:email/:token/:date', component: RegistrationComponent },
    { path: 'resetpassword/:username/:token/:expiration', component: RestorePasswordComponent },
    { path: 'resetpassword', component: RestorePasswordComponent },
    { path: 'createcompany', component: CreateCompanyComponent },
    { path: '', component: MyoMainComponent,
    canActivateChild:[UserService],
    children: [
        {
            path: 'facility',
            component: CompanyComponent
        },
        {
            path: 'facility/:facilityId',
            component: FacilityComponent
        },
        {
            path: 'facility/:facilityId/section/:sectionId',
            component: SectionComponent
        },
        {
            path: 'facility/:facilityId/section/:sectionId/senior/:seniorId',
            component: SeniorComponent,
            canDeactivate: [SeniorService]
        },
        {
          path: 'employees',
          component: EmployeesComponent
        },
        {
          path: 'employees/:employeeId/:userRole',
          component: EmployeeComponent
        },
        {
          path: 'posts',
          component: PostsComponent
        },
        {
          path: 'administrators',
          component: AdminsComponent,
          canDeactivate: [UserService]
        }
     ]},
    { path: '**', redirectTo: '/login', pathMatch: 'full' },
 ];

@NgModule({
	imports: [ MyoMainModule, RouterModule.forRoot(appRoutes), RouterModule.forChild(appRoutes) ],
  	exports: [ RouterModule ]
})
export class AppRoutingModule { }
