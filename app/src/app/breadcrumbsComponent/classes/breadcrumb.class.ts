export class Breadcrumb {
  name: string;
  link: string;
  active: boolean;

  constructor() {

  }
}
