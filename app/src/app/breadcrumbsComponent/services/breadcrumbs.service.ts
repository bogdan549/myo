import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

import { Breadcrumb } from '../classes/breadcrumb.class';

@Injectable({
    providedIn: 'root'
})
export class BreadcrumbsService {

    private rootStep: Breadcrumb = {
        name: 'Login',
        link: '/login',
        active: false
    };

    private breadcrumbs: Breadcrumb[] = [this.rootStep];

    private stepsSubject: BehaviorSubject<Breadcrumb[]> = new BehaviorSubject(this.breadcrumbs);  

    setStep(index: number, name: string, link: string): void {
        if (index < 0) return;
        for (let i = 0; i < index; i++) {
            if (!this.breadcrumbs[i]) return;
            this.breadcrumbs[i].active = true;
        }

        this.breadcrumbs[index] = ({ name: name, link: link, active: false });
        this.breadcrumbs.length = index + 1;
        this.stepsSubject.next(this.breadcrumbs);
    }

    getSteps(): Observable<Breadcrumb[]> {
        return this.stepsSubject.asObservable();
    }

    constructor() { }
}
