import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { Router, Event, NavigationEnd } from '@angular/router';

import { BreadcrumbsService } from './services/breadcrumbs.service';
import { Breadcrumb } from './classes/breadcrumb.class';

@Component({
    selector: 'app-breadcrumbs',
    templateUrl: './breadcrumbs.component.html',
    styleUrls: ['./breadcrumbs.component.css']
})
export class BreadcrumbsComponent implements OnInit {

    isLoggedIn: boolean = false;
    breadcrumbs: Breadcrumb[];
    private subscription: Subscription = new Subscription();

    constructor(private breadcrumbsService: BreadcrumbsService,
            private router: Router) { }

    ngOnInit() {
        this.getSteps();


        this.router.events.subscribe((event: Event) => {
            if (event instanceof NavigationEnd) {
                if(this.router.url.search(/login/) === -1 &&
                        this.router.url.search(/register/) === -1 &&
                        this.router.url.search(/resetpassword?/) === -1 &&
                        this.router.url.search(/createcompany?/) === -1 ){
                    this.isLoggedIn = true;
                } else {
                    this.isLoggedIn = false;
                }
            }

        })
    }

    getSteps(): void {
        this.subscription.add(
            this.breadcrumbsService.getSteps().subscribe(result => {
                this.breadcrumbs = result;
            })
        );
    }

    ngOnDestroy() {
		this.subscription.unsubscribe();
	}

}
